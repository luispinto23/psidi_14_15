const fs = require("fs");
const PDFDocument = require('pdfkit');

/************/
// data
/************/

//TODO pass these as parameters to the module
const port = process.env.PORT || 3002;
const SERVER_ROOT = "http://localhost:" + port;

const carrier_url = 'http://localhost:3006/shipping';

const preco_por_foto = 10;
const printer_address = "Porto";
const printer_id = "1";
const precoTransporte = 10;
const backofficePw 	= "backoffice/";

// DATA STORE

var order =  {};

var photos = {};

// SAMPLE DATA

var now = new Date();
var yesterday = now.getDate() + 1;
var jan1st2014 = new Date(2014, 01, 01);
var may2nd2014 = new Date(2014, 05, 02);

order['1'] = {printer_id: 1,
    id_encomenda: 1,
    user: 'Mary',
    confirmed: 0,
    print_price: 15,
    transport_price: precoTransporte,
    address: "Rua Dr. António Bernardino de Almeida, 431 4200-072 Porto Portugal",
    country: 'Portugal',
    city: 'Porto', photos:[{id_foto: "1",id_album: "Primavera"}]};


photos['Mary'] = {path: "girl.jpg"};


//helper functions

function buildOrder(orderID, userID, photosDoUser, orcamento, country, city, statusOrder)
{
    return {
        printer_id: printer_id,
        id_encomenda: orderID,
        user: userID,
        confirmed: statusOrder,
        print_price: orcamento,
        transport_price: precoTransporte,
        address: printer_address,
        country: country,
        city: city,
        photos:photosDoUser
    };
}

function getNewOrderID(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size + 1;
}


function handleGetService(req, res) { /*ALTERAR PARAR ENQUADRAR COM A FUNÇÃO - MADE IN MADALENA*/

    console.log("-<<-handleGetService->>-");

    console.log(req.body);
    /*
    console.log("    order.id= "+req.body.id);
    console.log("    order.country= "+req.body.country);
    console.log("    order.city= " + req.body.city);
    console.log("    req = " + req);
    console.log("    res = " + res);
    */

    var entry = {
    	id_printer_shop : printer_id,
    	address : printer_address,    	
    	price_print : preco_por_foto
    };

   

        var orderID = getNewOrderID(order);

        entry = buildOrder(req.body.id, req.body.country, req.body.city); // alterar
        order[orderID] = entry;
        console.log("oder >> " + order[orderID].id);

        res.status(200).send(entry);
 
};


function getArrayUrls(photosDoUser, userID) {
    var requestURLS = [];
    photosDoUser.forEach(function (item) {
        var albumID = item.id_album;
        var photoID = item.id_foto;

        var url = "http://localhost:3001/user/" + userID + '/album/' + albumID + '/photo/' + photoID;
        requestURLS.push(url);
    })
    return requestURLS;
}
function handlePostService(req, res) { /*ALTERAR PARAR ENQUADRAR COM A FUNÇÃO - MADE IN MADALENA*/


    var entry = order[req.body];

    if (entry === undefined) {

        //console.log("fotos array" + req.body.photos);

        var orderID = getNewOrderID(order);
        var userID = req.body.id;
        var photosDoUser = req.body.photos;
        var orcamento = photosDoUser.length * preco_por_foto;
        var country = req.body.country;
        var city = req.body.city;
        var statusOrder = 0; // 0 - não confirmado; 1 - confirmado;

        var orderEntry = buildOrder(orderID, userID, photosDoUser, orcamento, country, city, statusOrder);

        order[orderID] = orderEntry;
        console.log("oder >> " + order[orderID].id);
        /*
        * Os proximos req.body devem incluir os id's das fotos de modo d preecher o array
        * var linksPhotos = [];
        * linksPhotos = {1,2,3,4,...};
        * var orçamento = 0.0;
        * while(linkPhotos.size()>0)
        *   orçamento = orçamento + preco_por_foto;
        * res.status(201).send(orçamento);
        */

    //generatePDFOfUser(res, entry, linksPhotos)





        //var requestURLS = getArrayUrls(photosDoUser, userID);
        //console.log(requestURLS);

        //generatePDFOfUser(res, entry, requestURLS)

        //res.status(201).json(entry);
        res.status(201).json(orderEntry);
    }
    else {
// Falta implementar
        res.status(404).send("Order " + req.body.order.orderID + " not found.");
    }
    /*var newID = "z" + (Math.random() * 1000).toString().substr(1, 4);
     messages[newID] = buildOrder(newID, req.body.text, req.body.user);
     res.status(201).set('Location', SERVER_ROOT + "/message/" + newID).json(messages[newID]);*/
};

function handleGetServiceItem(req, res) {

    console.log("request GET handlePutServiceItem ">>req);
    var entry = users[req.userID];
    console.log("»» requested: " + req.userID + " »» " + entry);
    if (entry === undefined) {
        res.status(404).send("User " + req.userID + " not found.");
    }
    else {
        res.format({
            'application/json': function(){
                res.json(entry);
            },
            'application/pdf': function(){
                console.log("»» generated PDF");
                generatePDFOfUser(res, entry, photos[req.userID]);
            },
            'default': function() {
                // log the request and respond with 406
                res.status(406).send('Not Acceptable');
            }
        });
    }
}
		
function handlePutServiceItem(req, res) {

    console.log("request PUT handlePutServiceItem ");
    console.log(req.body);

    var entry = order[req.body.id_encomenda];

    if (entry === undefined) {
        res.status(406).send('Undefined'); // ver relatório
    }
    else if(""+authEncode(backofficePw + req.body.id_encomenda) !=  req.body.authToken){
    	
        res.status(401).send('Unauthorized, invalid auth token '+req.body.authToken); // ver relatório

    }
    else{
    	console.log('Authorized, valid auth token '+req.body.authToken);
    	
    	//SIMULAR ATRASO A OBTER STATUS DO CARRIER
    	setTimeout(function(){
        var confirmedState = req.body.confirmed;
        var confirmada = confirmedState == 1 ? true:false;
        if(confirmada == true)
        {
            entry.confirmed = 1;

            var request = require('request');
            var buffer = "";
            var previsaoDeEntrega = 0;

            request
                .post(carrier_url, {form:{order_id : entry.id_encomenda, country:entry.country, city:entry.city, callbackMethod : req.body.callbackMethod, callbackUrl : req.body.callbackUrl, authToken : ""+authEncode(backofficePw + req.body.id_encomenda)}})
                .on('response', function(response) {
                    //console.log(response);
                    console.log(response.statusCode); // 200

                })
                .on("data", function (chunk) {
                    buffer += chunk;
                })
                .on('end', function (err) {
                	//CHAMAR CALLBACK DO BACKOFFICE
                    data = JSON.parse(buffer);
                    previsaoDeEntrega = data.previsao_dias;
	                var requestCallback = require('request');
	                var bufferCallback = "";
	                console.log("POSTING TO BO CALLBACK@"+getDateTime());	                
	            	requestCallback({
	          		  uri: req.body.callbackUrl,
	          		  method: req.body.callbackMethod,  //method: "POST",
	          		  form:{id_encomenda : req.body.id_encomenda, confirmed : 2 , address : req.body.morada , callbackMethod : req.body.callbackMethod , callbackUrl : req.body.callbackUrl, authToken : ""+authEncode(backofficePw + req.body.id_encomenda) }
	          		}, function(error, response, body) {
		                console.log("GETTING BO CALLBACK ORDER RESPONSE@"+getDateTime());
		                console.log("body");
	          		    console.log(body);

	          		});
                })
        }
        }, 5500);
        //FIM DE SIMULAR ATRASO
    	
    	//ENTRETANTO RESPONDER AO BACKOFFICE
    	console.log("Iniciar resposta para BackOffice enquanto se aguarda resposta do Carrier@"+getDateTime());
        res.status(200).send( { id_encomenda : entry.id_encomenda , previsao : "#PrinterShop num."+printer_id+" : Encomenda foi "+req.body.id_encomenda+" despachada para o Carrier@"+getDateTime()});	
    }

};


function handlePostServiceItem(req, res){
	//form:{id_encomenda : encomenda.id_encomenda, confirmed : 1 , callbackUrl : callbackAddress+"/"+encomenda.id_encomenda }
	
    console.log("request POST handlePutServiceItem ">>req);
    res.status(404).send( "not in use");

    
};
function handleDeleteServiceItem(req, res){


    console.log("request DELETE handlePutServiceItem ">>req);
    res.status(404).send( "not in use");
};



/*
 * 
 * PDF RESOURCE HANDLER
 * 
 * 
 * */

function handleGetServiceItemPdf(req, res) {
	console.log("»» request ItemPdf : " + req);
	var entry = req.serviceID;
	console.log("»» requested ItemPdf : " + req.serviceID + " »» " + entry);
	if (entry === undefined) {
		res.status(404).send("Album " + req.serviceID + " not found.");		
	}
	else {
		
		/*
		 * GET PDF
		 * 
		 * */
		var urlBackOffice = "http://localhost:3001/user/Mary/encomenda/"+req.serviceID;
		var async = require('async');
		async.parallel([
		                function(callback){
		                    setTimeout(function(){
		                    	
		                    	var http = require("http");

		                    	var request = http.get(urlBackOffice, function (response) {

		                    	    var buffer = "",
		                    	        data;
		                    	    response.on("data", function (chunk) {
		                    	        buffer += chunk;
		                    	    });

		                    	    response.on("end", function (err) {

		                    	        data = JSON.parse(buffer);

		                    	        console.log("Url BO Response");
		                    	        console.log(data);

		    	                        callback(null, data);

		                    	    });
		                    	});
		                    }, 25000);
		                    
		                },
		            ],
		            
		            function(err, results){
	        			console.log("results");
		                console.log(results);

		                
		                
	   
		 });
		
		
		
		/*
		 * 
		 * END GET PDF ENCOMENDA
		 * 
		 * 
		 * */
		
		
	    res.format({
	        'application/json': function(){
	            res.json(entry);
	        },
	        'application/pdf': function(){
	        	console.log("»» generated PDF");
	        	generatePdfAlbum(res, entry, photos[req.userID]);
	        },
	        'default': function() {
	            // log the request and respond with 406
	            res.status(406).send('Not Acceptable');
	        }
	    });
	}
}

function handlePutServiceItemPdf(req, res) {


    res.status(405).send( " cannot issue put to pdf " );


};

function handlePostServiceItemPdf(){
    res.status(405).send( " cannot issue post to pdf " );

	
};
function handleDeleteServiceItemPdf(){
    res.status(405).send( " cannot issue delete to pdf " );

	
};

function authEncode(cleanStr){
	
	return new Buffer(cleanStr).toString('base64');
	
}

function authDecode(dirtyStr){
	
	return new Buffer(dirtyStr, 'base64').toString('ascii')
}

function getDateTime() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;

}


function generatePdfAlbum(res, entry, photo){
	var doc = new PDFDocument();
	
	// stream the content to the http response
	res.setHeader("Content-Type", "application/pdf");
	doc.pipe(res);

	// document already has one page so let's use it
	
	//photo
	if (photo) {
		doc.image(__dirname + "/photo/" + photo.path);
		doc.moveDown();
	}

	//name
	doc.fontSize(18);
	doc.fillColor('black').text(entry.name);
	doc.moveDown();
	
	//email
	doc.fontSize(12);
	doc.fillColor('blue').text(entry.email);
	doc.moveDown();

	// close document and response stream
	doc.end();
}







/*
function generatePDFOfUser(res, entry, photos){
    var doc = new PDFDocument();

    // stream the content to the http response
    res.setHeader("Content-Type", "application/pdf");
    doc.pipe(res);

    // document already has one page so let's use it

    //photo
    if (photos) {

        photos.forEach(function(item) {
            doc.image(item);
            doc.moveDown();
        })

    }

    //name
    doc.fontSize(18);
    doc.fillColor('black').text(entry.name);
    doc.moveDown();

    //email
    doc.fontSize(12);
    doc.fillColor('blue').text(entry.email);
    doc.moveDown();

    // close document and response stream
    doc.end();
}*/

/////////////////////////////
// MODULE EXPORTS

exports.handleGetService = handleGetService;
exports.handlePostService = handlePostService;

exports.handleGetServiceItem = handleGetServiceItem;
exports.handlePutServiceItem = handlePutServiceItem;
exports.handlePostServiceItem = handlePostServiceItem;
exports.handleDeleteServiceItem = handleDeleteServiceItem;

exports.handleGetServiceItemPdf = handleGetServiceItemPdf;
exports.handlePutServiceItemPdf = handlePutServiceItemPdf;
exports.handlePostServiceItemPdf = handlePostServiceItemPdf;
exports.handleDeleteServiceItemPdf = handleDeleteServiceItemPdf;
