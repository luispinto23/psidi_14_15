var express = require('express');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

var printershopHandling = require("./printerShop-handler");
///user/:userID/photo ---
app.route("/printershop/")
		.get(printershopHandling.handleGetService)
        .post(printershopHandling.handlePostService);
    ;


app.route("/printershop/:serviceID")
    .get(printershopHandling.handleGetServiceItem)
    .put(printershopHandling.handlePutServiceItem)
    .post(printershopHandling.handlePostServiceItem)
    .delete(printershopHandling.handleDeleteServiceItem);

app.param('serviceID', function(req, res, next, serviceID){
    req.serviceID = serviceID;
    console.log("serviceID");
    console.log(req.serviceID);
    return next()
});

app.route("/printershop/:serviceID/pdf")
.get(printershopHandling.handleGetServiceItemPdf)
.put(printershopHandling.handlePutServiceItemPdf)
.post(printershopHandling.handlePostServiceItemPdf)
.delete(printershopHandling.handleDeleteServiceItemPdf);




/*app.route("/printershop/payload/:serviceID")
    .get(printershopHandling.handleGetServiceItem)
    .put(printershopHandling.handlePutServiceItem)
    .post(printershopHandling.handlePostServiceID)
    .delete(printershopHandling.handleDeleteServiceItem);
*/

// Facultativo
/*
app.route("/printershop/services/:userID")
    .get(printershopHandling.handleGetOrderItem)
    .put(printershopHandling.handlePutOrderItem)
    .post(printershopHandling.handlePostOrderItem)
    .delete(printershopHandling.handleDeleteOrderItem);
*/

///STARTING ...
var port = process.env.PORT || 3004;

app.listen(port, function() {
    console.log("Printer Shop with User Album to Printing and PDF creator\n");
    console.log("Listening on " + port);
});