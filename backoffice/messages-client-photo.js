///////////////////////////////////////////////////////
//
// Paulo Gandra de Sousa, Alexandre Bragança
// PSIDI / MEI / ISEP
// (c) 2014
//
///////////////////////////////////////////////////////


//
// this examples GETs one user from the server and checks if the user has a photo (HEAD)
// if so it will download the photo (GET) and a PDF card of the user
// otherwise it will POST a photo to the server
//

//
// WARNING
//
// this code violates the REST principles that URIs are opaque, as the client 
// knows how to build the URL to post the photo.
// it should use hypermedia instead.
//

var request = require('request');
var fs = require('fs');

var serverUrl = process.argv[2] || "http://localhost:3001/user/";
var userId = process.argv[3] || "Tom";
var photo = process.argv[4] || "man.jpg";


function checkIfUserHasPhoto(userId, photo) {
    // HEAD user/:uid/photo
    request({
         uri : serverUrl + userId + "/photo",
         method: "HEAD",
         json : {}
    },
    function(err, res, body){
        if (!err){
          if (res.statusCode == 404) {
            uploadPhoto(userId, photo);
          }
          else {
            downloadPhotoAndPDF(userId);
          }
        }
        else {
          console.log(err);
        }
    });
}

function uploadPhoto(userId, photo) {
  var req = request.post(serverUrl + userId + "/photo", 
    function (err, resp, body) {
      if (err) {
        console.log('Error!');
      } else {
        console.log('URL: ' + body);
      }
    });
    var form = req.form();
    form.append('displayImage', fs.createReadStream(photo));
}

function downloadPhotoAndPDF(userId) {
  // GET photo
  request
    .get(serverUrl + userId + "/photo")
    .on('error', function(err){
          console.log(err);
        })
    .pipe(fs.createWriteStream(userId + '-photo.jpg')); // TODO give proper file extension

  // GET PDF
  request({
     uri : serverUrl + userId,
     headers : {'Accept' : 'application/pdf'},
     })
    .on('error', function(err){
          console.log(err);
        })
    .pipe(fs.createWriteStream(userId + ".pdf"));
}


// GET User
request({
   uri : serverUrl + userId,
   json : {}
   }, 
   function(err, res, body){
      if (!err) {
        console.log("received: " + JSON.stringify(body));
        checkIfUserHasPhoto(userId, photo);
      }
      else{
        console.log(err);
      }
});
