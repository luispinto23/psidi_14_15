
//
// a node module to handle the /album/ resource and the /photo/ subresource
//


const fs = require("fs");
const PDFDocument = require('pdfkit');
const mkdirp  = require('mkdirp');


/************/
// data
/************/

//TODO pass these as parameters to the module
const port = process.env.PORT || 3001;
const SERVER_ROOT = "http://localhost:" + port;
const backofficePw  	= "backoffice/";


var serviceHandling 	= require("./ServiceGateway");

// DATA STORE

var albuns = {};
var encomendas = {};

// SAMPLE DATA

var now = new Date();
var yesterday = now.getDate() + 1;
var jan1st2014 = new Date(2014, 01, 01);
var may2nd2014 = new Date(2014, 05, 02);

albuns['Mary'] = [  {id: "Primavera", name:"Primavera", description:"Primavera ... ",  period_begin : yesterday, period_end : now, photos : [{ path: "0.png" },{ path: "1.png" },{ path: "2.png" }] },
					{id: "Verao",  name:"Verao",  description:"Verao ... ",   period_begin : yesterday, period_end : yesterday, photos : []},
					{id: "Outono",  name:"Outono",  description:"Outono ... ",   period_begin  :  yesterday, period_end : jan1st2014, photos : []},
					{id: "Inverno", name:"Inverno", description:"Inverno ... ",  period_begin :  yesterday, period_end : may2nd2014, photos : []}
		 ];
albuns['Ann'] = [  {id: "Album1", name:"Album1", description:"Album1 ... ",  period_begin : yesterday, period_end : now, photos : [] },
					{id: "Album2",  name:"Album2",  description:"Album2 ... ",   period_begin : yesterday, period_end : yesterday, photos : []}
		 ];

encomendas['Mary'] = [{	 id_encomenda: "1",

	 confirmed: "1",

	 print_price: "20",

	 transport_price: "12",

	 status: "em espera de stock",

	 address: "Rua Dr. António Bernardino de Almeida, 431 4200-072 Porto Portugal",
	 
	 printAlbuns : 
		 
		 [ { id_print: "1",

			 print_download_url: "/users/Mary/encomenda/1",

			 theme: "férias",

			 print_price: "7",

			 download_available: "1",

			 open_msg: "Album das férias para impressão",

			 fotos: [

			 {id_foto: "1",id_album: "Primavera"},

			 {id_foto: "2",id_album: "Primavera"}

			 ] },
		{
			 id_print: "2",

			 theme: "natal",

			 print_price: "4",

			 download_available: "0",

			 open_msg: "Album de Natal"

		}] ,
	 
	 createdOn : yesterday

	 }, {

	 id_encomenda: "2",
	 confirmed: "0",

	 print_price: "55",

	 transport_price: "10",

	 status: "em espera de confirmacao",

	 address: "Rua Dr. António Bernardino de Almeida, 431 4200-072 Porto Portugal",
	 
	 printAlbuns : [],
	 
	 createdOn : yesterday

	 }];


//
// handling the collection


function handleGetAlbuns(req, res) {
		res.json(albuns[req.userID]);
	};

function handlePutAlbuns(req, res) {
		res.status(405).send("Cannot overwrite the entire collection.");
	};

function handlePostAlbuns(req, res) {
		res.status(405).send("To create a new album, please issue a PUT to /album/:id.");
	};

function handleDeleteAlbuns(req, res) {
		res.status(405).send("Cannot delete the entire collection.");
	};

//
// handling individual itens in the collection
//



	function generatePDFOfAlbum(res, entry, photo){
		var doc = new PDFDocument();
		
		// stream the content to the http response
		res.setHeader("Content-Type", "application/pdf");
		doc.pipe(res);

		// document already has one page so let's use it
		
		//photo
		if (photo) {
			doc.image(__dirname + "/photo/" + photo.path);
			doc.moveDown();
		}

		//name
		doc.fontSize(18);
		doc.fillColor('black').text(entry.name);
		doc.moveDown();
		
		//description
		doc.fontSize(12);
		doc.fillColor('blue').text(entry.description);
		doc.moveDown();

		// close document and response stream
		doc.end();
	}

	function handleGetAlbumItem(req, res) {
			//console.log(req);

			var entry = req.albumID;
						
			console.log(entry);
			console.log("»» requested: " + req.albumID + " »» " + req.userID);
			
			if (entry === undefined) {
				res.status(404).send("album " + req.albumID + " not specified.");		
			}
			else {
				var userAlbuns = albuns[req.userID];
				var found = false;
				for(var i = 0; i < userAlbuns.length; i++) {
					var albumObj = userAlbuns[i];
					console.log("albumObj");
					console.log(albumObj);
					console.log("req.albumId");
					console.log(req.albumID);
				    if(albumObj.id === req.albumID){
				    	entry = albumObj;
				    	found = true;
				    	break;
				    }
				}
				if(found == true){
				    res.format({
				        'application/json': function(){
				            res.json(entry);
				        },
				        'application/pdf': function(){
				        	console.log("»» generated PDF");
				        	generatePDFOfAlbum(res, entry, albuns[req.albumID]);
				        },
				        'default': function() {
				            // log the request and respond with 406
				            res.status(406).send('Not Acceptable');
				        }
				    });
				}else{
					res.status(404).send("album " + req.albumID + " not found.");
					
				}
			}
		};

	function handlePutAlbumItem(req, res) {

			var entry = albuns[req.userID];

			console.log("init");
			console.log(req.albumID);
			var now = new Date();
			//SE USER NÃO ENCONTRADO
			if (entry === undefined) {
				entry = {
					id : req.albumID ,
					name : (req.body.name == undefined ? req.albumID : req.body.name),
					description : req.body.description, 
					createdOn : now,
					roles : req.body.roles,
					photos : (req.body.photos == undefined ? [] : req.body.photos),
				};
				albuns[req.userID] = entry;
				res.status(401).set('Location', SERVER_ROOT + "/user/" + req.userID+ "/album/" + req.albumID).json(entry);
			}
			//SE USER ENCONTRADO
			else {
				var userAlbuns = albuns[req.userID];
				var foundAlbum = false;
				for(var i = 0; i < userAlbuns.length; i++) {
					var albumObj = userAlbuns[i];
					console.log("albumObj");
					console.log(albumObj);
					console.log("req.albumId");
					console.log(req.albumID);
				    if(albumObj.id === req.albumID){
				    	foundAlbum = true;				 
				    	albumObj.name  		 = req.body.name === undefined ? albumObj.name : req.body.name;
				    	albumObj.description = req.body.description   === undefined ? albumObj.description : req.body.description;	
				    	albumObj.period_begin   = req.body.period_begin  === undefined ? albumObj.period_begin : req.body.period_begin ;	
				    	albumObj.period_end 	= req.body.period_end 	  === undefined ? albumObj.period_end :  req.body.period_end;		
				    	albumObj.photos 	= req.body.photos 	  === undefined ? albumObj.photos :  req.body.photos;	 
				    	userAlbuns[i] = albumObj;
				    	albuns[req.userID] = userAlbuns;
				    	break;
				    }
				}
				if(foundAlbum == false){
					entry = {
							id : req.albumID,
							name : (req.body.name == undefined ? req.albumID : req.body.name),
							description : req.body.description, 
							createdOn : now,
							roles : req.body.roles,
							photos : (req.body.photos == undefined ? [] : req.body.photos),
						};
						userAlbuns = albuns[req.userID];
						//userAlbuns[req.albumID] = entry;
						console.log(userAlbuns.length);
						userAlbuns[userAlbuns.length] = entry;
						albuns[req.userID] = userAlbuns;
						res.status(201).set('Location', SERVER_ROOT + "/user/" + req.userID+ "/album/" + req.albumID).json(entry);
						
					
				}
			
			}
			//console.log("albuns");
			//console.log(albuns);
		};

	function handlePostAlbumItem(req, res) {
			var entry = albuns[req.albumID];
			if (entry === undefined) {
				res.status(404).send("album " + req.albumID + " not found.");
			}
			else {
				entry.name = req.body.name;
				entry.description = req.body.description, 
				entry.photos = (req.body.photos == undefined ? [] : req.body.photos),
				res.json(entry);
			}
		};

	function handleDeleteAlbumItem(req, res) {
			var entry = albuns[req.albumID];
			if (entry === undefined) {
				res.status(404).send("album " + req.albumID + " not found.");
			}
			else {
				delete albuns[req.albumID];
				res.status(200).send("album " + req.albumID + " deleted.");
			}
		};


	//
	// handling photo subresource

	const ROOT_DIR = __dirname; 

	
	
	/*
	 * PHOTO COLLECTION
	 * 
	 * 
	 * */
	function handleGetAlbumItemPhotoCollection(req, res) {
		
		res.status(405).send("Please issue a GET for the album, then Get the desired photos");
  		
	};
	
	function handlePutAlbumItemPhotoCollection(req, res) {
		res.status(405).send("Cannot overwrite the entire collection.");
	};

	function handlePostAlbumItemPhotoCollection(req, res) {
		
			handlePostAlbumItemPhoto(req, res);	
			//res.status(405).send("To create a new foto, please issue a PUT to /photo/:id.");
		};
	
	function handleDeleteAlbumItemPhotoCollection(req, res) {
			res.status(405).send("Cannot delete the entire collection.");
		};
	
	
	/*
	 * PHOTO ITEM
	 * 
	 * 
	 * */
	
	function handleGetAlbumItemPhoto(req, res) {
			var entry = albuns[req.userID];
			var photoFilename;
			var hasAlbum = findAlbum(req.userID,req.albumID);
			
			
			console.log("»» requested photo: " + req.albumID + " »» " + "/ " + req.photoID + ">>>" + entry);
			if (entry === undefined) {
				res.status(404).send("user " + req.userID + " not found.");	
				return;
			}
			if (hasAlbum === undefined) {
				res.status(404).send("album " + req.albumID + " not found.");	
				return;
			} 
			if(hasAlbum.photos[req.photoID] === undefined){
				res.status(404).send("foto " + req.photoID + " not found.");
				return;
			}
			else{
				photoFilename = hasAlbum.photos[req.photoID].path;
			}


				console.log("foto:");
				console.log(ROOT_DIR + "/photoalbumdir/" +req.userID  + "/album/" + req.albumID + "/" + photoFilename);
				var options = {
				    root: ROOT_DIR + "/photoalbumdir/" +req.userID  + "/album/" + req.albumID + "/",
				    dotfiles: 'deny',
				    headers: {
				        'x-timestamp': Date.now(),
				        'x-sent': true
				    }
				  };
			      console.log("hasAlbum.photos");
			      console.log(hasAlbum.photos);
				  var fileName = hasAlbum.photos[req.photoID].path;
				  if (fileName !== undefined) {
					  res.sendFile(fileName, options, function (err) {
					    if (err) {
					      console.log(err);
					      res.status(err.status).end();
					    }
					    else {
					      console.log('Sent:', fileName);
					    }
					  });			  	
				  }
				  else {
				  	res.status(404).send("Photo of album  " + req.albumID + " not found.");		
				  }
	  		
		};

	function handlePutAlbumItemPhoto(req, res) {
			res.status(405).send("Cannot PUT a photo. Please use POST.");
		};

	function handlePostAlbumItemPhoto(req, res) {

		console.log("handlePostAlbumItemPhoto >>> "+ req);

				var hasAlbum = findAlbum(req.userID,req.albumID);
				
				mkdirp(ROOT_DIR + "/photoalbumdir/" +req.userID  + "/album/" + req.albumID + "/" , function (err) {
				    if (err) console.error(err)
				    else console.log('pow!')
				});
				
				var filename = req.files.displayImage.path; // TODO check if file has been sent
				var ext = filename.substr(filename.lastIndexOf('.'));
				var photoFilename = hasAlbum.photos.length + ext;
				var photoPath = ROOT_DIR + "/photoalbumdir/" +req.userID  + "/album/" + req.albumID + "/" + photoFilename;
			
				fs.rename(filename, photoPath, function(err){
					if (!err) {
						hasAlbum.photos[hasAlbum.photos.length] = { path: photoFilename};
						res.status(201).send("Photo of album " + hasAlbum.photos.length + ext + " added.");
					}
					else {
						res.status(500).send(err);	
					}
				});
				
				
			};

	function handleDeleteAlbumItemPhoto(req, res) {
			var entry = albuns[req.albumID];
			if (entry === undefined) {
				res.status(404).send("album " + req.albumID + " not found.");
			}
			else {
				if (photos[req.albumID] !== undefined) {
					delete photos[req.albumID];
					res.status(200).send("Photo of album " + req.albumID + " deleted.");
				}
				else {
					res.status(404).send("Photo of album  " + req.albumID + " not found.");		
				}
			}
		};


		
		
		

		

	//
	// handling the collection

	function handleGetEncomendas(req, res) {
		console.log("handleGetEncomendas called");
			res.json(encomendas[req.userID]);
		};

	function handlePutEncomendas(req, res) {
			res.status(405).send("Cannot overwrite the entire collection.");
		};

	function handlePostEncomendas(req, res) {
			res.status(405).send("To create a new encomenda, please issue a PUT to /encomenda/:id.");
		};

	function handleDeleteEncomendas(req, res) {
			res.status(405).send("Cannot delete the entire collection.");
		};

		
		

	//
	// handling individual itens in the collection
	//
	// encomenda id must be present in the request object
	//
	// GET 		return specific encomenda or 404
	// POST 	update existing entry or 404
	// PUT 		overwrite existing or create new given the id. 
	// DELETE 	deletes the encomenda - history is kept.
	//




		function handleGetEncomendaItem(req, res) {
			//console.log(req);
				var entry = req.encomendaID;
						
				var userEncomendas = encomendas[req.userID];
				console.log(entry);
				console.log("»» requested: " + req.encomendaID + " »» " + req.userID);
				
				if (entry === undefined) {
					res.status(404).send("encomenda " + req.encomendaID + " not specified.");		
				} else if(userEncomendas === undefined){
					res.status(404).send("encomendas " + req.encomendaID + " not found.");		
					
				}
				else {	
					
						var found = false;
						var encomendaObj ;
						for(var i = 0; i < userEncomendas.length; i++) {
							encomendaObj = userEncomendas[i];
							console.log("encomendaObj");
							console.log(encomendaObj);
							console.log("req.encomendaId");
							console.log(req.encomendaID);
						    if(encomendaObj.id_encomenda === req.encomendaID){
						    	entry = encomendaObj;
						    	found = true;							
						    	break;
						    }
						}
						
						if(found == true){
							
						    res.format({
						        'application/json': function(){
						            res.json(entry);
						        },
						        'default': function() {
						            // log the request and respond with 406
						            res.status(406).send('Not Acceptable');
						        }
						    });
						    
						} else {				
						
							res.status(404).send("encomenda " + req.encomendaID + " not found.");
							
						}
					
			
				}
			};

		function handlePutEncomendaItem(req, res) {
				
				var entry = encomendas[req.userID];
				var transport_price ;
				var print_price;
				transport_price = serviceHandling.getDistanceAndPrice(req.body.address,"Lisbon");
				print_price = transport_price;
				
				console.log("init");
				console.log(req.encomendaID);
				console.log("body");
				console.log(req.body);
				var now = new Date();
				//SE ENCOMENDAS DO USER NÃO ENCONTRADAS
				if (entry === undefined) {
					entry = {
						id_encomenda : req.encomendaID,
						confirmed : req.body.confirmed,
						print_price : print_price, 
						transport_price : transport_price, 
						status : req.body.status, 
						address : req.body.address, 
						printAlbuns : req.body.printAlbuns, 
						createdOn : now,
					};
					encomendas[req.userID] = entry;
					res.status(201).set('created 1st Order! Location', SERVER_ROOT + "/user/" + req.userID+ "/encomenda/" + req.encomendaID).json(entry);
       		
    			}
				//SE ENCOMENDA ENCONTRADA
				else {
					var userEncomendas = encomendas[req.userID];
					var foundAlbum = false;
					var encomendaObj;
					for(var i = 0; i < userEncomendas.length; i++) {
						encomendaObj = userEncomendas[i];

						console.log(req.encomendaID);
					    if(encomendaObj.id_encomenda === req.encomendaID){
							console.log("encomendaObj.address");
							console.log(encomendaObj.address);
							var transport_price = serviceHandling.getDistanceAndPrice(encomendaObj.address,"Lisbon");
							var print_price = transport_price;
							console.log("transport_price");
							console.log(transport_price);
					    	foundAlbum = true;				 
					    	encomendaObj.confirmed  		 = (req.body.confirmed === undefined ? encomendaObj.confirmed : req.body.confirmed );
					    	encomendaObj.print_price = (print_price   === undefined ? encomendaObj.print_price : print_price);	
					    	encomendaObj.transport_price   = (transport_price  === undefined ? encomendaObj.transport_price : transport_price );	
					    	encomendaObj.status   =( req.body.status  === undefined ? encomendaObj.status : req.body.status );	
					    	encomendaObj.address   = (req.body.address  === undefined ? encomendaObj.address : req.body.address );	
					    	encomendaObj.printAlbuns   = (req.body.printAlbuns  === undefined ? encomendaObj.printAlbuns : req.body.printAlbuns );	
					    	encomendaObj.createdOn 	=( req.body.createdOn 	  === undefined ? encomendaObj.createdOn :  req.body.createdOn);	 
					    	userEncomendas[i] = encomendaObj;
					    	encomendas[req.userID] = userEncomendas;
					    	console.log("encomendaObj updated "+req.body.confirmed);
					    	console.log(encomendaObj);
					    	
					    	//SE ENCOMENDA CONFIRMADA
					    	if(encomendaObj.confirmed === 1 || encomendaObj.confirmed === "1"){
					    		
						    	setTimeout(function(){
					                
									var orcamento = serviceHandling.getBestPrintShop(encomendaObj);
									
									console.log("----------------distancia melhor printer--------------------");
									//console.log(orcamento);			
						    	}, 20000);		
					    		
					    	}
					    	
					    	break;
					    }
					}
					if(foundAlbum == false){

						var transport_price = serviceHandling.getDistanceAndPrice(req.body.address,"Lisbon");
						var print_price = transport_price;
						entry = {
								id_encomenda : req.encomendaID,
								confirmed : req.body.confirmed,
								print_price :print_price, 
								transport_price : transport_price, 
								status : req.body.status, 
								address : req.body.address, 
								printAlbuns : req.body.printAlbuns, 
								createdOn : now,
							};
							userEncomendas = encomendas[req.userID];

							userEncomendas[userEncomendas.length] = entry;
							encomendas[req.userID] = userEncomendas;
							res.status(201).set('Location', SERVER_ROOT + "/user/" + req.userID+ "/encomenda/" + req.encomendaID).json(entry);
							
						
					}else {
				    	//SE ENCOMENDA CONFIRMADA
				    	if(encomendaObj.confirmed === 1 || encomendaObj.confirmed === "1"){
				    		
					    	setTimeout(function(){
				                
								var orcamento = serviceHandling.getBestPrintShop(encomendaObj);
								
								console.log("----------------distancia melhor printer--------------------");
								//console.log(orcamento);			
					    	}, 20000);		
							res.status(200).set('Updated & Confirmed ! Location', SERVER_ROOT + "/user/" + req.userID+ "/encomenda/" + req.encomendaID).json(entry);

				    	}
						/*if(encomendaObj.confirmed === 1 || encomendaObj.confirmed === "1"){
						serviceHandling.getDistanceAndPrice(encomendaObj.address,"Lisbon");
						res.status(200).set('Updated & Confirmed ! Location', SERVER_ROOT + "/user/" + req.userID+ "/encomenda/" + req.encomendaID).json(entry);

						}
						*/
						else{
						res.status(200).set('Updated ! Location', SERVER_ROOT + "/user/" + req.userID+ "/encomenda/" + req.encomendaID).json(entry);
						}	
					}
				
				}

			};


		function handlePatchEncomendaItem(req, res) {
			console.log("handlePatchEncomendaItem "+getDateTime());
			console.log(req.body);
					var entry = findEncomenda(req.userID,req.encomendaID);//encomendas[req.encomendaID];
					if (entry === undefined) {
						res.status(404).send("encomenda " + req.encomendaID + " not found.");
						
					} else if(""+serviceHandling.authEncode(backofficePw + req.encomendaID) !=  req.body.authToken){
						
						console.log("not valid auth Token "+""+serviceHandling.authEncode(backofficePw + req.encomendaID));
				        res.status(401).send('Unauthorized, invalid auth token '+req.body.authToken); // ver relatório

				    }
					else {				
						
						console.log("valid auth Token "+""+serviceHandling.authEncode(backofficePw + req.encomendaID));
						var confirmStatus = Number(req.body.confirmed);
						entry.confirmed = confirmStatus;
						updateEncomenda(req.userID,req.encomendaID,entry);
						console.log(entry);
						
				    	//SE ENCOMENDA CONFIRMADA CLIENTE
				    	if(confirmStatus === 1 || confirmStatus === "1"){
				    		
					    	setTimeout(function(){

								console.log("----------------encomenda confirmada pelo cliente - orçamentar--------------------");

								var orcamento = serviceHandling.getBestPrintShop(entry);
								
					    	}, 2000);		
				    		
				    	}
				    	
				    	//SE ENCOMENDA CONFIRMADA POR BACKOFFICE
				    	else if(confirmStatus === 2 || confirmStatus === "2"){
				    		
					    	setTimeout(function(){
								console.log("----------------encomenda aceite e despachada para o carrier, segundo o printershop--------------------");
					    	}, 2000);		
				    		
				    	}
				    	
				    	//SE ENCOMENDA CONFIRMADA ENTREGUE
				    	else if(confirmStatus === 3 || confirmStatus  === "3"){
				    		
					    	setTimeout(function(){
				                								
								console.log("----------------encomenda em andamento, segundo o carrier--------------------");

					    	}, 2000);		
				    		
				    	}
				    	
				    	//SE ENCOMENDA CONFIRMADA ENTREGUE
				    	else if(confirmStatus === 4 || confirmStatus  === "4"){
				    		
					    	setTimeout(function(){
				                								
								console.log("----------------encomenda entregue, segundo o carrier--------------------");

					    	}, 2000);		
				    		
				    	}
						res.json(entry);
					}
		};
				
				
		function handlePostEncomendaItem(req, res) {
				var entry = encomendas[req.encomendaID];
				if (entry === undefined) {
					res.status(404).send("encomenda " + req.encomendaID + " not found.");
				}
				else {
					entry.name = req.body.name;
					entry.confirmada = req.body.confirmada, 
					entry.roles = req.body.roles;
					res.json(entry);
				}
			};
			
		function handleDeleteEncomendaItem(req, res) {
				var entry = encomendas[req.encomendaID];
				if (entry === undefined) {
					res.status(404).send("encomenda " + req.encomendaID + " not found.");
				}
				else {
					delete encomendas[req.encomendaID];
					res.status(200).send("encomenda " + req.encomendaID + " deleted.");
				}
			};
	
			
			//
			// handling printAlbum subresource
			//
			// album id must be present in the request object
			//
			// GET 		return specific album's printAlbum or 404
			// POST 	upload (create or update) a album's printAlbum or 404
			// PUT 		not allowed 
			// DELETE 	deletes the album's printAlbum
			//

			
			/*
			 * PRINT ALBUM COLLECTION
			 * 
			 * 
			 * */
		function handleGetPrintAlbumCollection(req, res) {
				
				res.status(405).send("Please issue a GET for the Order, then Get the desired printAlbuns");
		  		
			};
			
			function handlePutPrintAlbumCollection(req, res) {
				res.status(405).send("Cannot overwrite the entire collection.");
			};

			function handlePostPrintAlbumCollection(req, res) {
					res.status(405).send("To create a new foto, please issue a PUT to /PrintAlbum/:id.");
				};
			
			function handleDeletePrintAlbumCollection(req, res) {
					res.status(405).send("Cannot delete the entire collection.");
				};
			
				
				/*
				 * PRINT ALBUM ITEM 
				 * 
				 * 
				 * */
		
			function handleGetPrintAlbumItem(req, res) {
				var entry = encomendas[req.userID];
				
				console.log("req.encomendaID");
				console.log(req.encomendaID);
				console.log("req.userID");
				console.log(req.userID);
				console.log("req.printAlbumID");
				console.log(req.printAlbumID);

				var hasPrintAlbum = findPrintAlbum(req.userID,req.encomendaID,req.printAlbumID)
				console.log("hasPrintAlbum");
				console.log(hasPrintAlbum);
				
				console.log("»» requested encomenda: " + req.encomendaID + " »» printAlbum " + "/ " + req.printAlbumID + ">>>" + entry);
				if (entry === undefined) {
					res.status(404).send("encomenda " + req.encomendaID  + " not found.");		
				}
				if (hasPrintAlbum === undefined) {
					res.status(404).send("printAlbum " + req.printAlbumID + " not found.");		
				} 
				else {
										
						res.format({
							'application/json': function(){
								res.json(hasPrintAlbum);
							},
							'default': function() {
								// log the request and respond with 406
								res.status(406).send('Not Acceptable');
							}
						});
				
				}
			};
			
			function handlePostPrintAlbumItem(req, res) {
				res.status(405).send("Cannot PUT a printAlbum. Please use PUT.");
			};
			
			//TODO DELETE handleDeletePrintAlbumItem
			function handleDeletePrintAlbumItem(req, res) {
				var entry = encomendas[req.encomendaID];
				if (entry === undefined) {
					res.status(404).send("album " + req.encomendaID + " not found.");
				}
				else {
					if (printAlbums[req.encomendaID] !== undefined) {
						delete printAlbums[req.encomendaID];
						res.status(200).send("Photo of album " + req.encomendaID + " deleted.");
					}
					else {
						res.status(404).send("Photo of album  " + req.encomendaID + " not found.");		
					}
				}
			};
			
			function handlePutPrintAlbumItem(req, res) {
				var entry = encomendas[req.userID];
				
				var hasPrintAlbum = findPrintAlbum(req.userID,req.encomendaID,req.printAlbumID)
				
				console.log("»» requested encomenda: " + req.encomendaID + " »» printAlbum " + "/ " + req.printAlbumID + ">>>" + entry);
				if (entry === undefined) {
					res.status(404).send("encomenda " + req.encomendaID  + " not found.");		
				}
				if (hasPrintAlbum === undefined || hasPrintAlbum === null) {
					var userEncomendas = findEncomenda(req.userID,req.encomendaID);
					
					userEncomendas.printAlbuns[userEncomendas.printAlbuns.length] = 
						
					  {
							id_print : req.body.id_print,
							print_download_url : req.body.print_download_url,
							theme : req.body.theme, 
							print_price : req.body.print_price, 
							download_available : req.body.download_available, 
							open_msg : req.body.open_msg, 
							fotos : req.body.fotos, 
							createdOn : now,
						};
					
					res.status(201).send("Print album " + userEncomendas.printAlbuns[userEncomendas.printAlbuns.length-1] + " added.");	
					
				} 
				else {
					
					hasPrintAlbum = {
							
							id_print : (req.body.id_print === undefined ? hasPrintAlbum.id_print : req.body.id_print),
							print_download_url : (req.body.print_download_url  === undefined ?  hasPrintAlbum.print_download_url : req.body.print_download_url) , 
							theme : (req.body.theme  === undefined ?  hasPrintAlbum.theme : req.body.theme ), 
							print_price : (req.body.print_price === undefined ?  hasPrintAlbum.print_price : req.body.print_price ), 
							download_available : ( req.body.download_available === undefined ?  hasPrintAlbum.download_available : req.body.download_available ) , 
							open_msg : (req.body.open_msg  === undefined ?  hasPrintAlbum.open_msg : req.body.open_msg ), 
							fotos : (req.body.fotos === undefined ?  hasPrintAlbum.fotos : req.body.fotos) , 
							createdOn : now ,
							
					};

				
					var userEncomendasAux = encomendas[req.userID];
					for(var i = 0; i < userEncomendasAux.length; i++) {
						
						var encomendaObj = userEncomendasAux[i];
						
					    if(encomendaObj.id_encomenda === req.encomendaID){
					    	
					    	for(var j = 0; j < encomendaObj.printAlbuns.length; j++) {
								
								var printAlbumObj = encomendaObj.printAlbuns[j];
								
							    if(printAlbumObj.id_print === req.printAlbumID){
																		
							    	encomendaObj.printAlbuns[j] = hasPrintAlbum;
							    	
							    	userEncomendasAux[i] = encomendaObj;
							    	
							    	 encomendas[req.userID] = userEncomendasAux;
							    	 							    	 
							    	 break;
							    }
							}
							
					    }
					}
					
						res.format({
							'application/json': function(){
								res.json(hasPrintAlbum);
							},
							'default': function() {
								// log the request and respond with 406
								res.status(406).send('Not Acceptable');
							}
						});
				
				}
			};
			
			/////////////////////////////
			// MODULE UTIL FUNCTIONS
				
			function findAlbum(userID,albumID){
				
				var userAlbuns = albuns[userID];
				var foundAlbum = false;
				for(var i = 0; i < userAlbuns.length; i++) {
					
					var albumObj = userAlbuns[i];
					
				    if(albumObj.id === albumID){
						console.log("albumObj");
						console.log(albumObj);
						console.log("req.albumId");
						console.log(albumID);
				    	foundAlbum = true;			 
				    	
				    	return albumObj;
				    }
				}
				if(foundAlbum == false){

					return	null;
					
				}
				
			}	
			
			function updateEncomenda(userID,encomendaID, encomendaUpd){
				//console.log("@findEncomenda");
				var userEncomendas = encomendas[userID];
				var foundOrder = false;
				for(var i = 0; i < userEncomendas.length; i++) {
					
					var encomendaObj = userEncomendas[i];
					
				    if(encomendaObj.id_encomenda == encomendaID){
					
						foundOrder = true;	
						userEncomendas[i] 	= encomendaUpd;
						encomendas[userID]	= userEncomendas;
				    	return encomendaObj;
				    }
				}
				if(foundOrder == false){

					return	null;
					
				}
				
			}
						
			function findEncomenda(userID,encomendaID){
				//console.log("@findEncomenda");
				var userEncomendas = encomendas[userID];
				var foundOrder = false;
				for(var i = 0; i < userEncomendas.length; i++) {
					
					var encomendaObj = userEncomendas[i];
					
				    if(encomendaObj.id_encomenda == encomendaID){

						foundOrder = true;			 
				    	console.log();
				    	return encomendaObj;
				    }
				}
				if(foundOrder == false){

					return	null;
					
				}
				
			}
			
			function findPrintAlbum(userID,encomendaID,printAlbumID){
				console.log("@findPrintAlbum");
				var userEncomenda = findEncomenda(userID,encomendaID);
				
				var foundPrint = false;
				for(var i = 0; i < userEncomenda.printAlbuns.length; i++) {
					
					var printAlbumObj = userEncomenda.printAlbuns[i];
					
				    if(printAlbumObj.id_print === printAlbumID){
						console.log("printAlbumObj");
						console.log(printAlbumObj);
						console.log("req.printAlbumID");
						console.log(printAlbumID);
						foundPrint = true;	
						
				    	return printAlbumObj;
				    }
				}
				if(foundPrint == false){

					return	null;
					
				}
				
			}
			
			
			
			function getDateTime() {

			    var date = new Date();

			    var hour = date.getHours();
			    hour = (hour < 10 ? "0" : "") + hour;

			    var min  = date.getMinutes();
			    min = (min < 10 ? "0" : "") + min;

			    var sec  = date.getSeconds();
			    sec = (sec < 10 ? "0" : "") + sec;

			    var year = date.getFullYear();

			    var month = date.getMonth() + 1;
			    month = (month < 10 ? "0" : "") + month;

			    var day  = date.getDate();
			    day = (day < 10 ? "0" : "") + day;

			    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;

			}
		
	/////////////////////////////
	// MODULE EXPORTS

	exports.handleGetAlbuns = handleGetAlbuns;
	exports.handlePutAlbuns = handlePutAlbuns;
	exports.handlePostAlbuns = handlePostAlbuns;
	exports.handleDeleteAlbuns = handleDeleteAlbuns;

	exports.handleGetAlbumItem = handleGetAlbumItem;
	exports.handlePostAlbumItem = handlePostAlbumItem;
	exports.handlePutAlbumItem = handlePutAlbumItem;
	exports.handleDeleteAlbumItem = handleDeleteAlbumItem;
	

	exports.handleGetAlbumItemPhotoCollection = handleGetAlbumItemPhotoCollection;
	exports.handlePutAlbumItemPhotoCollection = handlePutAlbumItemPhotoCollection;
	exports.handlePostAlbumItemPhotoCollection = handlePostAlbumItemPhotoCollection;
	exports.handleDeleteAlbumItemPhotoCollection = handleDeleteAlbumItemPhotoCollection;
	
	exports.handleGetAlbumItemPhoto = handleGetAlbumItemPhoto;
	exports.handlePostAlbumItemPhoto = handlePostAlbumItemPhoto;
	exports.handlePutAlbumItemPhoto = handlePutAlbumItemPhoto;
	exports.handleDeleteAlbumItemPhoto = handleDeleteAlbumItemPhoto;

	exports.handleGetEncomendas = handleGetEncomendas;
	exports.handlePutEncomendas = handlePutEncomendas;
	exports.handlePostEncomendas = handlePostEncomendas;
	exports.handleDeleteEncomendas = handleDeleteEncomendas;

	
	exports.handleGetEncomendaItem = handleGetEncomendaItem;
	exports.handlePostEncomendaItem = handlePostEncomendaItem;
	exports.handlePutEncomendaItem = handlePutEncomendaItem;
	exports.handlePatchEncomendaItem = handlePatchEncomendaItem;
	exports.handleDeleteEncomendaItem = handleDeleteEncomendaItem;
	
	exports.handleGetPrintAlbumCollection = handleGetPrintAlbumCollection;
	exports.handlePutPrintAlbumCollection	= handlePutPrintAlbumCollection;
	exports.handlePostPrintAlbumCollection	= handlePostPrintAlbumCollection;		
	exports.handleDeletePrintAlbumCollection	= handleDeletePrintAlbumCollection;
	
	
	exports.handleGetPrintAlbumItem = handleGetPrintAlbumItem;
	exports.handlePutPrintAlbumItem = handlePutPrintAlbumItem;
	exports.handlePostPrintAlbumItem = handlePostPrintAlbumItem;
	exports.handleDeletePrintAlbumItem = handleDeletePrintAlbumItem;
	
