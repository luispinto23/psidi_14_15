/**
 * BackOffice ServiceGateway
 */

const printerShop1Url = "http://localhost:3002/printershop";
const printerShop2Url = "http://localhost:3003/printershop";
const printerShop3Url = "http://localhost:3004/printershop";
const backofficePw  	= "backoffice/";
const callbackAddress = "http://localhost:3001/user/Mary/encomenda";

const accessKey = "AIzaSyBC5PSbbXeXIkQ-WQZMNQS3DmOdXbkitqc";// "AIzaSyDPrq4mkJ9wSJgnv6Ngdy2LSbmb76pjIzs";

const url = "https://maps.googleapis.com/maps/api/distancematrix/json?";
	  url += "origins=Vancouver+BC|Seattle";
	  url +="&destinations=San+Francisco|Victoria+BC&mode=driving&language=en-EN&key="+accessKey ;

  var geocoderProvider = 'google';
  var httpAdapter = 'https';
  // optional
  var extra = {
      apiKey: accessKey, 
      formatter: null         // 'gpx', 'string', ...
  };	  
	  
	  
function getDistanceAndPrice(origem, destino){
	
	var kmValue;
		
	var geocoder = require('node-geocoder').getGeocoder(geocoderProvider, httpAdapter, extra);
	var distance = require('google-distance-matrix');

	//var origins = ['San Francisco CA', '40.7421,-73.9914'];
	//var destinations = ['New York NY', 'Montreal', '41.8337329,-87.7321554', 'Honolulu'];
	var origins = [origem];
	var destinations = [destino];

	distance.key(accessKey);
	distance.units('metric');

	            	  distance.matrix(origins, destinations, function (err, distances) {
	            		    if (err) {
	            		        return console.log(err);
	            		    }
	            		    if(!distances) {
	            		        return console.log('no distances');
	            		    }
	            		    if (distances.status == 'OK') {
	            		        for (var i=0; i < origins.length; i++) {
	            		            for (var j = 0; j < destinations.length; j++) {
	            		                var origin = distances.origin_addresses[i];
	            		                var destination = distances.destination_addresses[j];
	            		                if (distances.rows[0].elements[j].status == 'OK') {
	            		                    var distance = distances.rows[i].elements[j].distance.value;
	            		                    kmValue = distance;
	            							console.log("------------------distance---------------------------");
	            		                    console.log('Distancia de ' + origin + ' até '  + destination + ' : ' + distance + " km");
	            							console.log("o preco e: "+calcPegadaCarbono(kmValue));
	            		                 } else {
	            		                    console.log(destination + ' is not reachable by land from ' + origin);
	            		                }
	            		            }
	            		        }
	            		    }
	            		});
	                
						return kmValue;
	   
	
	/*
	console.log("------------batchGeocode---------------------------------");


	geocoder.batchGeocode(['13 rue sainte catherine', '29 champs elysée paris'], function (values) {
	    console.log(values) ;
	});
	
	
	console.log("------------------Geocode---------------------------");

	geocoder.geocode('29 champs elysée paris')
    .then(function(res) {
        console.log(res);
    })
    .catch(function(err) {
        console.log(err);
    });		

	console.log("---------------Geo Reverse------------------------------");
	geocoder.reverse(45.767, 4.833)
    .then(function(res) {
        console.log(res);
    })
    .catch(function(err) {
        console.log(err);
    });
    
	*/
	
}




function getBestPrintShop(encomenda) {
	var distancia ;
    var async = require('async');
	async.parallel([
	                function(callback){
	                    setTimeout(function(){
	                    	
	                    	var http = require("http");

	                    	var request = http.get(printerShop1Url, function (response) {

	                    	    var buffer = "",
	                    	        data;
	                    	    response.on("data", function (chunk) {
	                    	        buffer += chunk;
	                    	    });

	                    	    response.on("end", function (err) {

	                    	        data = JSON.parse(buffer);
	                    	       
	                    	        console.log(data);

	    	                        callback(null, data);

	                    	    });
	                    	});
	                    }, 2500);
	                    
	                },
	                function(callback){
	                	

	                    setTimeout(function(){
	                    	var http = require("http");

	                    	var request = http.get(printerShop2Url, function (response) {

	                    	    var buffer = "",
	                    	        data;
	                    	    response.on("data", function (chunk) {
	                    	        buffer += chunk;
	                    	    });

	                    	    response.on("end", function (err) {

	                    	        data = JSON.parse(buffer);
	                    	       
	                    	        console.log(data);

	    	                        callback(null, data);

	                    	    });
	                    	});
	                    }, 2500);
	                },
	                function(callback){
	                	

	                    setTimeout(function(){
	                    	var http = require("http");

	                    	var request = http.get(printerShop3Url, function (response) {

	                    	    var buffer = "",
	                    	        data;
	                    	    response.on("data", function (chunk) {
	                    	        buffer += chunk;
	                    	    });

	                    	    response.on("end", function (err) {

	                    	        data = JSON.parse(buffer);
	                    	        callback(null, data);

	                    	    });
	                    	});
	                    }, 2500);
	                }
	            ],
	            
	            function(err, results){
        			console.log("results");
	                console.log(results);
	                var menor = 999999;
	                var morada = "";
	                
	                for(var i = 0; i < results.length; i++){
	                	var printRxp = results[i].transport_price;
	                		
	                	if (printRxp < menor){
	                		menor  = printRxp;
	                		morada = results[i].address;
	                		console.log("Preco menor encontrado");
	                		console.log(menor);
	                	}
	                	
	                }

	                console.log("encomenda.address,morada");
	                console.log(encomenda.address);
	                console.log(morada);

                	distancia = getDistanceAndPrice(encomenda.address,morada);
	                
	                
	                console.log("----------------------PARALLEL END--------------------------");
	                
	                //TODO POST ENCOMENDA MELHOR PRINTER
	                /*

			    	*/
	                console.log("encomenda.address -> "+ printerShop1Url+"/"+encomenda.id_encomenda);
	                console.log(encomenda.address);
	                
	                var request = require('request');
	                var buffer = "";
	                console.log("POSTING ORDER");
	                console.log(printerShop1Url+"/"+encomenda.id_encomenda);
	                
	            	request({
	          		  uri: printerShop1Url+"/"+encomenda.id_encomenda,
	          		  method: "PUT",  //method: "POST",
	          		  form:{id_encomenda : encomenda.id_encomenda, authToken : ""+authEncode(backofficePw+encomenda.id_encomenda), confirmed : 1 , address : morada , callbackMethod : "PATCH" , callbackUrl : callbackAddress+"/"+encomenda.id_encomenda }
	          		}, function(error, response, body) {
		                console.log("POSTING ORDER RESPONSE");
		                console.log("body");
	          		  	console.log(body);
	          		  	
	          		});
	            	
	            	/*
	                request
	                .put(printerShop1Url+"/"+encomenda.id_encomenda, {form:{id_encomenda : encomenda.id_encomenda, confirmed : 1 , callbackUrl : callbackAddress+"/"+encomenda.id_encomenda }})
	                .on('response', function(response) {
	                    console.log(response.statusCode) // 200

	                })
	                .on("data", function (chunk) {
	                    buffer += chunk;
	                })
	                .on('end', function (err) {
	                    data = JSON.parse(buffer);
	                    console.log("PRINTER RESPONSE");
	                    console.log(data);
	                    
	                    return data; 
	                })*/
	                
   
	            });

	
}


function authEncode(cleanStr){
	
	return new Buffer(cleanStr).toString('base64');
	
}

function authDecode(dirtyStr){
	
	return new Buffer(dirtyStr, 'base64').toString('ascii')
}

function calcPegadaCarbono(km){
	
	return (km * 0.26 / 1000);
}

function orderEncomenda(){

	  var menor = getPrintShopAddress();
	  return menor;
}



exports.getDistanceAndPrice = getDistanceAndPrice; //getDistance
exports.getBestPrintShop = getBestPrintShop;
exports.orderEncomenda = orderEncomenda;
exports.authEncode = authEncode;