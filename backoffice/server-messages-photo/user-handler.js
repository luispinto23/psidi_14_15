

const fs = require("fs");
const PDFDocument = require('pdfkit');

/************/
// data
/************/

//TODO pass these as parameters to the module
const port = process.env.PORT || 3001;
const SERVER_ROOT = "http://localhost:" + port;


// DATA STORE

var users =  {};

var photos = {};

// SAMPLE DATA

var now = new Date();
var yesterday = now.getDate() + 1;
var jan1st2014 = new Date(2014, 01, 01);
var may2nd2014 = new Date(2014, 05, 02);

users['Mary'] = {id: "Mary", name:"Mary", password:"psidi", email:"mary@contoso.com", roles:[], 		createdOn: now, 		city : "Lisbon", country : "Portugal",};
users['Ann'] =  {id: "Ann",  name:"Ann",  password:"psidi",email:"ann@contoso.com",  roles:['admin'], createdOn: yesterday, city : "Lisbon", country : "Portugal",	};
users['Tom'] =  {id: "Tom",  name:"Tom",  password:"psidi",email:"tom@contoso.com",  roles:[], 		createdOn: jan1st2014, 	city : "Lisbon", country : "Portugal",};
users['Carl'] = {id: "Carl", name:"Carl", password:"psidi",email:"carl@contoso.com", roles:[], 		createdOn: may2nd2014, city : "Lisbon", country : "Portugal",	};

photos['Mary'] = {path: "girl.jpg"};


//
// handling the collection


function handleGetUsers(req, res) {
		res.json(users);
	};

function handlePutUsers(req, res) {
		res.status(405).send("Cannot overwrite the entire collection.");
	};

function handlePostUsers(req, res) {
		res.status(405).send("To register a new user, please issue a PUT to /user/:id.");
	};

function handleDeleteUsers(req, res) {
		res.status(405).send("Cannot delete the entire collection.");
	};

//
// handling individual itens in the collection


function generatePDFOfUser(res, entry, photo){
	var doc = new PDFDocument();
	
	// stream the content to the http response
	res.setHeader("Content-Type", "application/pdf");
	doc.pipe(res);

	// document already has one page so let's use it
	
	//photo
	if (photo) {
		doc.image(__dirname + "/photo/" + photo.path);
		doc.moveDown();
	}

	//name
	doc.fontSize(18);
	doc.fillColor('black').text(entry.name);
	doc.moveDown();
	
	//email
	doc.fontSize(12);
	doc.fillColor('blue').text(entry.email);
	doc.moveDown();

	// close document and response stream
	doc.end();
}

function handleGetUserItem(req, res) {
		console.log("»» request: " + req);
		var entry = users[req.userID];
		console.log("»» requested: " + req.userID + " »» " + entry);
		if (entry === undefined) {
			res.status(404).send("User " + req.userID + " not found.");		
		}
		else {
		    res.format({
		        'application/json': function(){
		            res.json(entry);
		        },
		        'application/pdf': function(){
		        	console.log("»» generated PDF");
		        	generatePDFOfUser(res, entry, photos[req.userID]);
		        },
		        'default': function() {
		            // log the request and respond with 406
		            res.status(406).send('Not Acceptable');
		        }
		    });
		}
	};

function handlePutUserItem(req, res) {
		console.log(req)
		console.log(req.userID)
		var entry = users[req.userID];
		var now = new Date();
		if (entry === undefined) {
			entry = {
				id : req.userID,
				name : req.body.name,
				email : req.body.email, 
				password : req.body.password, 
				city : req.body.city, 
				country : req.body.country, 
				createdOn : now,
				roles : req.body.roles,
			};
			users[req.userID] = entry;
			res.status(201).set('Location', SERVER_ROOT + "/user/" + req.userID).json(entry);
		}
		else {
			entry.name = req.body.name;
			entry.email = req.body.email, 
			entry.password = req.body.password, 
			entry.roles = req.body.roles,
			entry.city = req.body.city, 
			entry.country = req.body.country, 
			res.json(entry);
		}
	};

function handlePostUserItem(req, res) {
		var entry = users[req.userID];
		if (entry === undefined) {
			res.status(404).send("User " + req.userID + " not found.");
		}
		else {
			entry.name = req.body.name;
			entry.email = req.body.email, 
			entry.password = req.body.password, 
			entry.roles = req.body.roles,
			entry.city = req.body.city, 
			entry.country = req.body.country, 
			res.json(entry);
		}
	};

function handleDeleteUserItem(req, res) {
		var entry = users[req.userID];
		if (entry === undefined) {
			res.status(404).send("User " + req.userID + " not found.");
		}
		else {
			delete users[req.userID];
			res.status(200).send("User " + req.userID + " deleted.");
		}
	};


//
// handling photo subresource
//
// user id must be present in the request object
//
// GET 		return specific user's photo or 404
// POST 	upload (create or update) a user's photo or 404
// PUT 		not allowed 
// DELETE 	deletes the user's photo
//

const ROOT_DIR = __dirname; 

function handleGetUserItemPhoto(req, res) {
		var entry = users[req.userID];
		console.log("»» requested photo: " + req.userID + " »» " + entry);
		if (entry === undefined) {
			res.status(404).send("User " + req.userID + " not found.");		
		}
		else {
			var options = {
			    root: ROOT_DIR + '/photo/',  // __dirname + '/photo/'
			    dotfiles: 'deny',
			    headers: {
			        'x-timestamp': Date.now(),
			        'x-sent': true
			    }
			  };

			  if (photos[req.userID] !== undefined) {
				  var fileName = photos[req.userID].path;
				  res.sendFile(fileName, options, function (err) {
				    if (err) {
				      console.log(err);
				      res.status(err.status).end();
				    }
				    else {
				      console.log('Sent:', fileName);
				    }
				  });			  	
			  }
			  else {
			  	res.status(404).send("Photo of user  " + req.userID + " not found.");		
			  }
  		}
	};

function handlePutUserItemPhoto(req, res) {
		res.status(405).send("Cannot PUT a photo. Please use POST.");
	};

function handlePostUserItemPhoto(req, res) {
		var entry = users[req.userID];
		console.log("»» POST photo: " + req.userID + " »» " + entry);

		if (entry === undefined) {
			res.status(404).send("User " + req.userID + " not found.");
		}
		else {
			var filename = req.files.displayImage.path; // TODO check if file has been sent
			var ext = filename.substr(filename.lastIndexOf('.'));
			var photoFilename = req.userID + ext;
			var photoPath = ROOT_DIR + "/photo/" + photoFilename;

			var photoEntry = photos[req.userID];
			if (photoEntry !== undefined) {
				console.log("»» updating existing photo");
				fs.rename(filename, photoPath, function(err){
					if (!err) {
						photos[req.userID].path = photoFilename;
						res.status(200).send("Ok");
					}
					else {
						res.status(500).send(err);	
					}
				}); 
			}
			else {
				console.log("»» creating new photo");
				fs.rename(filename, photoPath, function(err){
					if (!err) {
						photos[req.userID] = { path: photoFilename};
						res.status(201).send("Created");
					}
					else {
						res.status(500).send(err);	
					}
				});
			}
		}
	};

function handleDeleteUserItemPhoto(req, res) {
		var entry = users[req.userID];
		if (entry === undefined) {
			res.status(404).send("User " + req.userID + " not found.");
		}
		else {
			if (photos[req.userID] !== undefined) {
				delete photos[req.userID];
				res.status(200).send("Photo of User " + req.userID + " deleted.");
			}
			else {
				res.status(404).send("Photo of user  " + req.userID + " not found.");		
			}
		}
	};


/////////////////////////////
// MODULE EXPORTS

exports.handleGetUsers = handleGetUsers;
exports.handlePutUsers = handlePutUsers;
exports.handlePostUsers = handlePostUsers;
exports.handleDeleteUsers = handleDeleteUsers;

exports.handleGetUserItem = handleGetUserItem;
exports.handlePostUserItem = handlePostUserItem;
exports.handlePutUserItem = handlePutUserItem;
exports.handleDeleteUserItem = handleDeleteUserItem;

exports.handleGetUserItemPhoto = handleGetUserItemPhoto;
exports.handlePostUserItemPhoto = handlePostUserItemPhoto;
exports.handlePutUserItemPhoto = handlePutUserItemPhoto;
exports.handleDeleteUserItemPhoto = handleDeleteUserItemPhoto;
