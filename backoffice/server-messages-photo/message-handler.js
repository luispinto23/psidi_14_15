
//
// a node module to handle the /message/ resource
//


/************/
// data
/************/

//TODO pass these as parameters to the module
const port = process.env.PORT || 3001;
const SERVER_ROOT = "http://localhost:" + port;


// DATA STORE

var messages =  {};

// SAMPLE DATA

var now = new Date();
var yesterday = now.getDate() + 1;
var jan1st2014 = new Date(2014, 01, 01);
var may2nd2014 = new Date(2014, 05, 02);

messages['a1'] = {id: "a1", text:"Sample one", 		sender:"Mary", 	createdOn: now, 		updatedOn: now};
messages['b2'] = {id: "b2", text:"Sample two", 		sender:"Ann", 	createdOn: yesterday, 	updatedOn: now};
messages['c3'] = {id: "c3", text:"Sample three", 	sender:"Tom", 	createdOn: jan1st2014, 	updatedOn: now};
messages['d4'] = {id: "d4", text:"Sample four", 	sender:"Carl", 	createdOn: may2nd2014, 	updatedOn: now};

//
// helper functions
//

function buildMessage(newID, text, user){
	const now = new Date();
	return {
			id : newID, 
			text : text,
			sender : user, 
			createdOn : now,
			updatedOn : now,
		};
}

//
// handling the collection
//

function comparatorUser(message, user) {
	return message.sender === user;
}

function comparatorText(message, text) {
	return messages[m].text.indexOf(text) >= 0;
}

function filterMessages(messages, param, comparator) {
	var out = {};
	if (param !== undefined) {
		for (m in messages) {
			if (comparator(messages[m], param)) {
				out[messages[m].id] = messages[m];
			}
		}
	}
	else {
		out = messages;
	}
	return out;
}

function filterMessagesByUser(messages, user) {
	return filterMessages(messages, user, comparatorUser);
}

function filterMessagesByText(messages, text) {
	return filterMessages(messages, text, comparatorText);
}

function handleGetMessages(req, res) {
		var out = filterMessagesByUser(messages, req.query.user);
		out = filterMessagesByText(out, req.query.text);
		res.json(out);
	};

function handlePutMessages(req, res) {
		res.status(405).send("Cannot overwrite the entire collection.");
	};

function handlePostMessages(req, res) {
		var newID = "z" + (Math.random()*1000).toString().substr(1, 4);
		messages[newID] = buildMessage(newID, req.body.text, req.body.user);
		res.status(201).set('Location', SERVER_ROOT + "/message/" + newID).json(messages[newID]);
	};

function handleDeleteMessages(req, res) {
		res.status(405).send("Cannot delete the entire collection.");
	};

//
// handling individual itens in the collection


function handleGetMessageItem(req, res) {
		var entry = messages[req.messageID];
		console.log("»» requested: " + req.messageID + " »» " + entry);
		if (entry === undefined) {
			res.status(404).send("Message " + req.messageID + " not found.");		
		}
		else {
			res.json(entry);
		}
	};

function handlePutMessageItem(req, res) {
		var entry = messages[req.messageID];
		if (entry === undefined) {
			entry = buildMessage(req.messageID, req.body.text, req.body.user);
			messages[req.messageID] = entry;
		    res.status(201).set('Location', SERVER_ROOT + "/message/" + req.messageID).json(entry);
	    }
		else {
			entry.text = req.body.text;
			entry.updatedOn = new Date();
			res.json(entry);
		}
	};

function handlePostMessageItem(req, res) {
		var entry = messages[req.messageID];
		if (entry === undefined) {
			res.status(404).send("Message " + req.messageID + " not found.");
		}
		else {
			entry.text = req.body.text;
			var now = new Date();
			entry.updatedOn = now;
			res.json(entry);
		}
	};

function handleDeleteMessageItem(req, res) {
		var entry = messages[req.messageID];
		if (entry === undefined) {
			res.status(404).send("Message " + req.messageID + " not found.");
		}
		else {
			delete messages[req.messageID];
			res.status(204).send("Message " + req.messageID + " deleted.");
		}
	};



/////////////////////////////
// MODULE EXPORTS

exports.handleGetMessages = handleGetMessages;
exports.handlePutMessages = handlePutMessages;
exports.handlePostMessages = handlePostMessages;
exports.handleDeleteMessages = handleDeleteMessages;

exports.handleGetMessageItem = handleGetMessageItem;
exports.handlePostMessageItem = handlePostMessageItem;
exports.handlePutMessageItem = handlePutMessageItem;
exports.handleDeleteMessageItem = handleDeleteMessageItem;
