
var express = require('express');
var multer = require('multer'); // for multipart file upload via form
var bodyParser = require('body-parser');
//var methodOverride = require('method-override');

var messageHandling = require("./message-handler");
var userHandling 	= require("./user-handler");
var albumHandling 	= require("./album-handler");

var app = express();

app.use(multer({ dest: './uploads/'}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	next();
});

//app.use(methodOverride());


// logging : DEBUG
//app.use(express.logger('dev'));



app.route("/message") 
	.get(messageHandling.handleGetMessages)
	.put(messageHandling.handlePutMessages)
	.post(messageHandling.handlePostMessages)
	.delete(messageHandling.handleDeleteMessages);

app.param('messageID', function(req, res, next, messageID){
  req.messageID = messageID;
  return next()
})

app.route("/message/:messageID") 
	.get(messageHandling.handleGetMessageItem)
	.put(messageHandling.handlePutMessageItem)
	.post(messageHandling.handlePostMessageItem)
	.delete(messageHandling.handleDeleteMessageItem);

//
// USER resource
//

app.route("/user") 
	.get(userHandling.handleGetUsers)
	.put(userHandling.handlePutUsers)
	.post(userHandling.handlePostUsers)
	.delete(userHandling.handleDeleteUsers);

app.param('userID', function(req, res, next, userID){
  req.userID = userID;
  return next()
})

app.route("/user/:userID") 
	.get(userHandling.handleGetUserItem)
	.put(userHandling.handlePutUserItem)
	.post(userHandling.handlePostUserItem)
	.delete(userHandling.handleDeleteUserItem);

//
//USER ALBUM resource
//

app.route("/user/:userID/album") 
	.get(albumHandling.handleGetAlbuns)
	.put(albumHandling.handlePutAlbuns)
	.post(albumHandling.handlePostAlbuns)
	.delete(albumHandling.handleDeleteAlbuns);

app.param('userID', function(req, res, next, userID){
req.userID = userID;
return next()
})

app.route("/user/:userID/album/:albumID") 
	.get(albumHandling.handleGetAlbumItem)
	.put(albumHandling.handlePutAlbumItem)
	.post(albumHandling.handlePostAlbumItem)
	.delete(albumHandling.handleDeleteAlbumItem);

app.param('albumID', function(req, res, next, albumID){
	  req.albumID = albumID;
	  return next()
	})

	
//
//USER ALBUM PHOTO resource
//
	
app.route("/user/:userID/album/:albumID/photo") 
	.get(albumHandling.handleGetAlbumItemPhotoCollection)
	.put(albumHandling.handlePutAlbumItemPhotoCollection)
	.post(albumHandling.handlePostAlbumItemPhotoCollection)
	.delete(albumHandling.handleDeleteAlbumItemPhotoCollection);

//
//USER ALBUM PHOTO resource
//
	
app.route("/user/:userID/album/:albumID/photo/:photoID") 
	.get(albumHandling.handleGetAlbumItemPhoto)
	.put(albumHandling.handlePutAlbumItemPhoto)
	.post(albumHandling.handlePostAlbumItemPhoto)
	.delete(albumHandling.handleDeleteAlbumItemPhoto);

app.param('photoID', function(req, res, next, photoID){
	  req.photoID = photoID;
	  return next()
	})
	
		
//
//USER ORDER resource
//

app.route("/user/:userID/encomenda") 
	.get(albumHandling.handleGetEncomendas)
	.put(albumHandling.handlePutEncomendas)
	.post(albumHandling.handlePostEncomendas)
	.delete(albumHandling.handleDeleteEncomendas);

app.param('userID', function(req, res, next, userID){
req.userID = userID;
return next()
})

app.route("/user/:userID/encomenda/:encomendaID") 
	.get(albumHandling.handleGetEncomendaItem)
	.put(albumHandling.handlePutEncomendaItem)
	.post(albumHandling.handlePostEncomendaItem)
	.patch(albumHandling.handlePatchEncomendaItem)
	.delete(albumHandling.handleDeleteEncomendaItem);

app.param('encomendaID', function(req, res, next, encomendaID){
	  req.encomendaID = encomendaID;
	  return next()
	})	
	

//
//USER ALBUM ENCOMENDA PRINTALBUM resource
//
	
	app.route("/user/:userID/encomenda/:encomendaID/printAlbum") 
	.get(albumHandling.handleGetPrintAlbumCollection)	
	.put(albumHandling.handlePutPrintAlbumCollection)
	.post(albumHandling.handlePostPrintAlbumCollection)
	.delete(albumHandling.handleDeletePrintAlbumCollection);	
	
	
app.route("/user/:userID/encomenda/:encomendaID/printAlbum/:printAlbumID") 
	.get(albumHandling.handleGetPrintAlbumItem)	
	.put(albumHandling.handlePutPrintAlbumItem)
	.post(albumHandling.handlePostPrintAlbumItem)
	.delete(albumHandling.handleDeletePrintAlbumItem);

app.param('printAlbumID', function(req, res, next, printAlbumID){
	  req.printAlbumID = printAlbumID;
	  return next()
	})
	
/////////////////////////////
// STARTING ...

var port = process.env.PORT || 3001;

app.listen(port, function() {
	console.log("MyPhotoAlbum@RestANode is Online!\n");
  console.log("Listening on " + port);
});