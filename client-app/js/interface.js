$(document).ready(function () {

    checkUserSession();

    $('input[type=file]').bootstrapFileInput();
    $('.file-inputs').bootstrapFileInput();

    document.getElementById("registration-password").onchange = validatePassword;
    document.getElementById("registration-password-confirmation").onchange = validatePassword;

    hideAllClasses('div-inicial');

});

$('#registration-password').on('keyup', function(e) {

    var passwordValue = $('#registration-password').val();

    $.get("http://localhost:8080/customerservice/MainService?pw="+passwordValue, function () {
        //alert( "success" );
    })
        .done(function (data, textStatus, jqXHR) {
            //alert( "second success" );
            /*console.log(jqXHR.status);
             console.log(textStatus);
             console.log(data);*/

            var passwordStrength = '';
            var passwordLength = 0;

            $.each(data, function (index, value) {
                /*console.log("index ->"+index)
                console.log("value ->"+value)*/

                if (index == 'strength') {
                    passwordStrength = value;
                }
                else if(index == 'length'){
                    passwordLength = value;
                }
            });
            passwordStrength = passwordStrength.trim();

            $('#password-strength-div > div').removeClass();

            if (passwordStrength == 'false') {
                if (passwordLength < 4) {
                    var passwordLabel = 'Weak';
                    $('#password-strength-div > div').addClass('progress-bar progress-bar-danger');
                    $('#password-strength-div > div').width('25%');
                    $('#password-strength-div > div > span').html(passwordLabel);

                } else {
                    var passwordLabel = 'Medium';
                    $('#password-strength-div > div').addClass('progress-bar progress-bar-warning');
                    $('#password-strength-div > div').width('50%');
                    $('#password-strength-div > div > span').html(passwordLabel);
                }
            } else {
                var passwordLabel = 'Strong';
                $('#password-strength-div > div').addClass('progress-bar progress-bar-success');
                $('#password-strength-div > div').width('100%');
                $('#password-strength-div > div > span').html(passwordLabel);
            }

            if(passwordLength > 0)
                $('#password-strength-div').show();
            else
                $('#password-strength-div').hide();

        })
        .fail(function (data, textStatus) {
            alert("error");
        })
        .always(function () {
            //alert( "finished" );
        });



});


$('.btn-login-action').click(function () {
    hideAllClasses('div-login');
});

$('.logout-action').click(function () {
    sessionStorage.clear();
    location.reload();
});

$('.btn-view-photos-menu').bind("click", function () {
    hideAllClasses('div-user-photos');
    $('.btn-back-menu').show();

    var user = getUserFromSession();

    getAlbumsFromUser(user.Username);
});

$(".btn-register-action").bind("click", function () {
    hideAllClasses('div-registration');
});

$(".btn-show-new-album-menu").bind("click", function () {
    hideAllClasses('div-create-albums');
    var user = getUserFromSession();
    userID = user.Username;

    var userPhotos = $.get("http://localhost:3001/user/" + userID + '/album', function () {
        //alert("success");

    })
        .done(function (data, textStatus, jqXHR) {
            //alert( "second success" );
            /*console.log(jqXHR.status);
             console.log(textStatus);
             console.log(data);*/

            if (jqXHR.status == 200) { // Sucesso
                showAlbumUploadList(data);
            }


            //console.log("userExists >>"+userExists)

        })
        .fail(function (data, textStatus) {
            alert("Ainda não tem qualquer foto na sua colecção.");
        })
        .always(function () {
            //alert( "finished" );
        });

    $('.btn-back-menu').show();
});

$(".btn-show-upload-menu").bind("click", function () {
    hideAllClasses('div-upload-photos');
    var user = getUserFromSession();
    userID = user.Username;

    var userPhotos = $.get("http://localhost:3001/user/" + userID + '/album', function () {
        //alert("success");

    })
        .done(function (data, textStatus, jqXHR) {
            //alert( "second success" );
            /*console.log(jqXHR.status);
             console.log(textStatus);
             console.log(data);*/

            if (jqXHR.status == 200) { // Sucesso
                showAlbumUploadList(data);
            }


            //console.log("userExists >>"+userExists)

        })
        .fail(function (data, textStatus) {
            alert("Ainda não tem qualquer foto na sua colecção.");
        })
        .always(function () {
            //alert( "finished" );
        });

    $('.btn-back-menu').show();
});

$(".back-div-inicial").bind("click", function () {
    hideAllClasses('user-workspace');
    $('.btn-back-menu').hide();
});

$("#form-signin").submit(function (event) {
    event.preventDefault();

    var username = this.username.value;
    var password = this.password.value;

    $.get("http://localhost:3001/user", function () {
        //alert( "success" );
    })
        .done(function (data, textStatus, jqXHR) {
            //alert( "second success" );
            /*console.log(jqXHR.status);
             console.log(textStatus);
             console.log(data);*/
            var userExists = false;

            var user;

            $.each(data, function (index, value) {

                //index == username ? userExists = true : false;
                if (index == username) {
                    userExists = true;
                    user = value
                }
            });

            //userExists == true ? viewLandingPage(username, password) : viewLoginPage(username);

            if (userExists == true)
                validatePasswordUser(user, username, password);
            else
                viewLoginPage(username);

            //console.log("userExists >>"+userExists)

        })
        .fail(function (data, textStatus) {
            alert("error: " + textStatus);
        })
        .always(function () {
            //alert( "finished" );
        });


});

$("#form-registration").submit(function (event) {
    event.preventDefault();
    var user = {
        id: this.username.value,
        name: this.username.value,
        password: this.password.value,
        email: this.email.value,
        city: this.city.value,
        country: this.country.value
    }

    //var passwordConfirmation = this.password_confirmation.value;
    $.get("http://localhost:3001/user", function () {
        //alert( "success" );
    })
        .done(function (data, textStatus, jqXHR) {
            //alert( "second success" );
            /*console.log(jqXHR.status);
             console.log(textStatus);
             console.log(data);*/
            var userExists = false;

            $.each(data, function (index, value) {
                index == user.username ? userExists = true : false;
            });

            userExists == true ? viewRegistrationPage(user.username, user.email) : postNewUser(user);

            //console.log("userExists >>"+userExists)

        })
        .fail(function (data, textStatus) {
            alert("error");
        })
        .always(function () {
            //alert( "finished" );
        });

});

$("#form-upload").submit(function (event) {
    event.preventDefault();

    var fileSelect = document.getElementById('file-select');

    var files = fileSelect.files;

    // Create a new FormData object.
    var formData = new FormData();

    // Loop through each of the selected files.
    for (var i = 0; i < files.length; i++) {
        var file = files[i];

        // Check the file type.
        if (!file.type.match('image.*')) {
            continue;
        }

        // Add the file to the request.
        formData.append('displayImage', file, file.name);
    }

    var albumToUpload = this.albumToUpload.value;

    postUserPhotos(albumToUpload, formData);

});

$("#form-submit-to-print").submit(function (event) {
    event.preventDefault();

    getSelectedPhotosToPrint();


});

$("#form-create-new-album").submit(function (event) {
    event.preventDefault();

    var albumID = this.newAlbumName.value;
    albumID = albumID.trim();

    var newAlbum = {
        id: albumID,
        name: this.newAlbumName.value,
        description: this.newAlbumDescription.value,
        period_begin: this.newAlbumPeriodStart.value,
        period_end: this.newAlbumPeriodEnd.value
    }

    postNewAlbum(newAlbum);


});

function getSelectedPhotosToPrint() {

    var arrayPhotos = [];

    var selectedPhotos = []; // Fotos para impressão
    $('input[name="photosToPrint"]:checked').each(function () {
        selectedPhotos.push(this.value);
    });

    console.log("photos para a encomenda ->" + selectedPhotos);

    $.each(selectedPhotos, function (index, value) {

        var filename = value.split('/').pop();
        var filename = filename.replace(/.[^.]+$/, '');

        var pathArray = value.split('/');

        var photoAlbum = pathArray[6];

        var toArray = {id_foto: filename ,id_album: photoAlbum };

        arrayPhotos.push(toArray)
    });


    var user = getUserFromSession();
    var userID = user.Username;
    var encomendaID = $('#new-photo-order-select').val();

    var URL = "http://localhost:3001/user/" + userID + '/encomenda/' + encomendaID;

    console.log('URL de request ->' + URL);

    var userEncomendas = $.get(URL, function () {
        //alert("success");

    })
        .done(function (data, textStatus, jqXHR) {
            //alert( "second success" );
            console.log(jqXHR.status);
            console.log(textStatus);
            console.log(data);

            if (jqXHR.status == 200) {
                var printalbunsEncomenda = data.printAlbuns;
                var numPrintAlbums = data.printAlbuns.length;
                numPrintAlbums++;


                putNewPrintAlbum(encomendaID, arrayPhotos, numPrintAlbums);

            }

        })
        .fail(function (data, textStatus) {
            //alert("Impossível encontrar os detalhes do album.");
        })
        .always(function () {
            //alert( "finished" );
        });
}

function putNewPrintAlbum(encomendaID, arrayPhotos, idPrintAlbum) {

    var user = getUserFromSession();
    var userID = user.Username;

    var printAlbumUrl = '/users/'+userID+'/encomenda/'+encomendaID+'/albumPrints/'+idPrintAlbum;


    var URL = "http://localhost:3001/user/" + userID + '/encomenda/' + encomendaID+'/printAlbum/'+idPrintAlbum;

    var newPrintAlbum = {
        id_print: idPrintAlbum,
        print_download_url: printAlbumUrl,
        fotos:arrayPhotos
    }

    console.log("new printalbum ->"+newPrintAlbum);

    $.ajax({
        url: URL,
        type: 'PUT',
        data: JSON.stringify(newPrintAlbum),
        contentType: 'application/json'
    })
        .done(function (data, textStatus, jqXHR) {
            //alert( "second success" );
            console.log(jqXHR.status);
            console.log(textStatus);
            console.log(data);

            if (jqXHR.status == 201) { // Sucesso
                alert("Album criado com sucesso.")
            }
            else {

                alert("Ocorreu um problema na criação do album.")
            }

        })
        .fail(function () {
            alert("error: " + jqxhr);
        })
        .always(function () {
            //alert( "finished" );
        });
}

function hideAllClasses(excepcao) {
    $('.div-inicial, .div-registration, .div-login, .user-workspace, .div-user-photos, .div-upload-photos, .back-div-inicial, .div-create-albums').hide();
    $('.' + excepcao).show();
}

function validatePasswordUser(user, username, password) {

    //console.log('ValidatePassword');
    //console.log(user.password);

    var dbPassword = user.password;

    dbPassword == password ? viewLandingPage(user) : viewLoginPage(username);


    //return true;
}

function postNewUser(obj) {

    var URL = 'http://localhost:3001/user/' + obj.id;
    $.ajax({
        url: URL,
        type: 'PUT',
        data: JSON.stringify(obj),
        contentType: 'application/json'
    })
        .done(function (data, textStatus, jqXHR) {
            //alert( "second success" );
            /*console.log(jqXHR.status);
             console.log(textStatus);
             console.log(data);*/

            if (jqXHR.status == 201) { // Sucesso
                viewLandingPage(obj)
            }
            else {
                viewRegistrationPage(obj.username, obj.email)
                alert('Ocorreu um erro ao efectuar o registo \n Por favor volte a tentar.');

            }

        })
        .fail(function () {
            alert("error: " + jqxhr);
        })
        .always(function () {
            //alert( "finished" );
        });

}

function postNewAlbum(obj) {

    var user = getUserFromSession();
    userID = user.Username;

    var URL = 'http://localhost:3001/user/' + userID + '/album/' + obj.id;

    $.ajax({
        url: URL,
        type: 'PUT',
        data: JSON.stringify(obj),
        contentType: 'application/json'
    })
        .done(function (data, textStatus, jqXHR) {
            //alert( "second success" );
            console.log("URL >>" + URL)
            console.log(jqXHR.status);
            console.log(textStatus);
            console.log(data);

            if (jqXHR.status == 201) { // Sucesso
                alert('Album ' + obj.name + ' created successfully.')
            }
            else {
                viewRegistrationPage(obj.username, obj.email)
                alert('Ocorreu um erro ao efectuar o registo \n Por favor volte a tentar.');

            }

        })
        .fail(function () {
            alert("error: " + jqXHR);
        })
        .always(function () {
            //alert( "finished" );
        });

}

function viewCreateAlbumPage(obj) {

    $('#new-album-description').val(obj.description);
    $('#new-album-period-start').val(obj.period_begin);
    $('#new-album-period-end').val(obj.period_end);
    alert('Album already exists!')
}

function viewRegistrationPage(username, email) {
    $('#registration-user').val(username);
    $('#registration-email').val(email);
    $('#registration-password').val('');
    $('#registration-password-confirmation').val('');
    $('.login-link').show();
    alert('Username already exists!')
}

function validatePassword() {
    var pass2 = document.getElementById("registration-password-confirmation").value;
    var pass1 = document.getElementById("registration-password").value;
    if (pass1 != pass2)
        document.getElementById("registration-password-confirmation").setCustomValidity("Passwords Don't Match");
    else
        document.getElementById("registration-password-confirmation").setCustomValidity('');
}

function viewLandingPage(user) {

    var loggedUser = new setCredentials(user);

    sessionStorage.setItem('loggedUser', JSON.stringify(loggedUser));

    hideAllClasses('user-workspace');
    $('.div-logout').show();
}

function viewLoginPage(username) {

    $('#signin-user').val(username);
    $('#signin-pass').val('');
    $('.register-link').show();

    alert('Incorrect Username or Password');

}

function checkUserSession() {
    if (getUserFromSession() == null) {
        hideAllClasses('div-inicial');
        return false;
    }
    return true;
}

function getUserFromSession() {
    var user = sessionStorage.getItem('loggedUser');
    var actualUser = $.parseJSON(user);
    return actualUser;
}

function setCredentials(user) {
    this.Username = user.id;
    this.Password = user.password;
    this.Country = user.country;
    this.City = user.city;
}

function getAlbumsFromUser(userID) {

    var userPhotos = $.get("http://localhost:3001/user/" + userID + '/album', function () {
        //alert("success");

    })
        .done(function (data, textStatus, jqXHR) {
            //alert( "second success" );
            /*console.log(jqXHR.status);
             console.log(textStatus);
             console.log(data);*/

            if (jqXHR.status == 200) { // Sucesso
                setAlbumList(data);
            }


            //console.log("userExists >>"+userExists)

        })
        .fail(function (data, textStatus) {
            alert("Ainda não tem qualquer foto na sua colecção.");
        })
        .always(function () {
            //alert( "finished" );
        });
}

function setAlbumList(objAlbums) {
    $('#album-list-group').html('');

    $.each(objAlbums, function (index, value) {
        var elemento = '<a href="#" class="list-group-item" id="' + value.id + '" onclick="getAlbumDetails(this.id)">' + value.name + '</a>';
        $('#album-list-group').append(elemento);
    });

    //console.log('aqui');
}

function showAlbumUploadList(objAlbums) {
    $('#upload-album-select').html('');

    $.each(objAlbums, function (index, value) {
        var elemento = '<option value="' + value.id + '">' + value.name + '</option>';

        $('#upload-album-select').append(elemento);
    });

    //console.log('aqui');
}

function getAlbumDetails(albumID) { // vai buscar as fotos do album

    var user = getUserFromSession();
    var userID = user.Username;
    var URL = "http://localhost:3001/user/" + userID + '/album/' + albumID;

    var userAlbum = $.get(URL, function () {
        //alert("success");

    })
        .done(function (data, textStatus, jqXHR) {
            //alert( "second success" );
            console.log(jqXHR.status);
            console.log(textStatus);
            console.log(data);

            if (jqXHR.status == 200) {
                var photosArray = data.photos;

                showAlbumPhotos(URL, photosArray);
            }

        })
        .fail(function (data, textStatus) {
            alert("Impossível encontrar os detalhes do album.");
        })
        .always(function () {
            //alert( "finished" );
        });
}

function showAlbumPhotos(albumURL, photosArray) {
    //$('#photo-album-gallery').html('XPTO');

    $('#photo-album-order').show();

    $('#photo-album-gallery').html('');
    $('#photo-album-submit-to-print').html('');
    if (photosArray.length > 0) {

        $.each(photosArray, function (index, value) {

            var trimmedPath = value.path.replace(/.[^.]+$/, '');

            var elemento = '<div class="col-xs-6 col-md-6"><input type="checkbox" class="checked-photo-to-print" name="photosToPrint" value="' + albumURL + '/photo/' + value.path + '"> Select to print<img src="' + albumURL + '/photo/' + trimmedPath + '" itemprop="thumbnail" alt="Image description" class="img-responsive"/></div>';


            $('#photo-album-gallery').append(elemento);
        });

        var printPhotosBtn = '<input class="btn btn-primary btn-large btn-print-photos-action" href="#" type="submit" value="Add Photos to order">';

        var user = getUserFromSession();
        var userID = user.Username;
        var URL = "http://localhost:3001/user/" + userID + '/encomenda';

        var userEncomendas = $.get(URL, function () {
            //alert("success");

        })
            .done(function (data, textStatus, jqXHR) {
                //alert( "second success" );
                console.log(jqXHR.status);
                console.log(textStatus);
                console.log(data);

                if (jqXHR.status == 200) {
                    setEncomendasList(data)
                }

            })
            .fail(function (data, textStatus) {
                //alert("Impossível encontrar os detalhes do album.");
            })
            .always(function () {
                //alert( "finished" );
            });

        $('#photo-album-submit-to-print').append(printPhotosBtn);
    }
    else {
        alert('No photos in the selected Album');
    }


}

function setEncomendasList(objEncomendas) {
    $('#new-photo-order-select').html('');

    $.each(objEncomendas, function (index, value) {
        var elemento = '<option value="' + value.id_encomenda + '">Encomenda ' + value.id_encomenda + '</option>';

        $('#new-photo-order-select').append(elemento);
    });

    var elemento = '<option value="novaEncomenda"> -- Nova Encomenda -- </option>';

    $('#new-photo-order-select').append(elemento);
}

/*function getPrintAlbumList(encomendaID){
 var user = getUserFromSession();
 var userID = user.Username;
 var URL = 'http://localhost:3001/user/'+userID+'/encomenda/'+encomendaID;

 var userEncomendas = $.get(URL, function () {
 //alert("success");

 })
 .done(function (data, textStatus, jqXHR) {
 //alert( "second success" );
 console.log(jqXHR.status);
 console.log(textStatus);
 console.log(data);

 if (jqXHR.status == 200) {
 setPrintAlbunsList(data)
 }

 })
 .fail(function (data, textStatus) {
 //alert("Impossível encontrar os detalhes do album.");
 })
 .always(function () {
 //alert( "finished" );
 });

 }*/

function setPrintAlbunsList(objPrintAlbuns) {
    $('#new-photo-print-select').html('');

    $.each(objPrintAlbuns, function (index, value) {
        var elemento = '<option value="' + value.id_print + '">PrintAlbum ' + value.id_print + '</option>';

        $('#new-photo-order-select').append(elemento);
    });
}

function postUserPhotos(albumID, obj) {

    console.log("aqui!");

    var user = getUserFromSession();
    var userID = user.Username;


    var URL = 'http://localhost:3001/user/' + userID + '/album/' + albumID + '/photo/';

    console.log(URL);
    $.ajax({
        url: URL,
        type: 'POST',
        data: obj,
        processData: false,
        contentType: false
    });


}