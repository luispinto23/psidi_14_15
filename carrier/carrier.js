var express = require('express');
var bodyParser = require('body-parser');
//var methodOverride = require('method-override');

var orderHandling = require("./order-handler");



var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
//app.use(methodOverride());


// logging : DEBUG
//app.use(express.logger('dev'));


//
// MESSAGE resource
//

app.route("/shipping")
    .post(orderHandling.handlePostOrders);

app.param('messageID', function(req, res, next, messageID){
    req.messageID = messageID;
    return next()
})

/*app.route("/order/:orderID")
    .get(orderHandling.handleGetOrderItem)
    .put(orderHandling.handlePutOrderItem)
    .post(orderHandling.handlePostOrderItem)
    .delete(orderHandling.handleDeleteOrderItem);*/

////////////////
// STARTING ...

var port = process.env.PORT || 3006;

app.listen(port, function() {
    console.log("Carrier handler with external API to wunderweather\n");
    console.log("Listening on " + port);
});
