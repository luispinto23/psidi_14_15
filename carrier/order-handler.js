

// Openweathermap - API Key - d07d43aa3933332bd4a7836b51e1d30c

// Exemplo de pedido - http://api.openweathermap.org/data/2.5/weather?q=Porto,pt&lang=pt&units=metric

//TODO pass these as parameters to the module
const port = process.env.PORT || 3006;
const SERVER_ROOT = "http://localhost:" + port;
const backofficePw 	= "backoffice/";


// DATA STORE

var orders = {};

// SAMPLE DATA

var now = new Date();
var yesterday = now.getDate() + 1;
var jan1st2014 = new Date(2014, 01, 01);
var may2nd2014 = new Date(2014, 05, 02);


orders['o1'] = {id: "o1", country: "Portugal", city: "Maia", createdOn: now, updatedOn: now};
orders['o2'] = {id: "o2", country: "Spain", city: "Madrid", createdOn: yesterday, updatedOn: now};
/*orders['o3'] = {id: "o3", country: "Italy", city: "Rome", createdOn: jan1st2014, updatedOn: now};
orders['o4'] = {id: "o4", country: "Portugal", city: "Porto", createdOn: may2nd2014, updatedOn: now};*/

//
// helper functions
//

function buildOrder(newID, country, city) {
    const now = new Date();
    return {
        id: newID,
        country: country,
        city: city,
        createdOn: now,
        updatedOn: now
    };
}





function getNewOrderID(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size + 1;
}

//
// handling the collection


function comparatorUser(message, user) {
    return message.sender === user;
}

function comparatorText(message, text) {
    return messages[message].text.indexOf(text) >= 0;
}

function filterOrders(messages, param, comparator) {
    var out = {};
    if (param !== undefined) {
        for (m in messages) {
            if (comparator(messages[m], param)) {
                out[messages[m].id] = messages[m];
            }
        }
    }
    else {
        out = messages;
    }
    return out;
}

function filterOrdersByUser(messages, user) {
    return filterOrders(messages, user, comparatorUser);
}

function filterOrdersByText(messages, text) {
    return filterOrders(messages, text, comparatorText);
}

function getOrdersUrls(obj) {
    var out = [];
    var urls = [];

    var keys = Object.keys(obj);
    for (var i = 0, length = keys.length; i < length; i++) {
        out.push(orders[keys[i]]);

    }

    for (var j = 0, length = out.length; j < length; j++) {
        var apiKey = '1c5b49f9c1fa2deb';
        var city = out[j].city;
        var country = out[j].country;
        var lang = out[j].lang ? out[j].lang : 'EN';

        var urlReq = "http://api.wunderground.com/api/" + apiKey + "/conditions/lang:" + lang + "/q/" + country + "/" + city + ".json";

        urls.push(urlReq);
    }

    return urls;
}

function handleGetOrders(req, res) {
    //res.json(orders);
    //var async = require('async');

    //var reqs = getOrdersUrls(orders);


    var out = [];
    var urls = [];

    var keys = Object.keys(orders);
    for (var i = 0, length = keys.length; i < length; i++) {
        out.push(orders[keys[i]]);

    }

    for (var j = 0, length = out.length; j < length; j++) {
        var apiKey = '1c5b49f9c1fa2deb';
        var city = out[j].city;
        var country = out[j].country;
        var lang = out[j].lang ? out[j].lang : 'EN';

        var urlReq = "http://api.wunderground.com/api/" + apiKey + "/conditions/lang:" + lang + "/q/" + country + "/" + city + ".json";

        urls.push(urlReq);
    }


    console.log("URLS -> "+urls);

    var async = require('async');
    var request = require('request');
    async.map(urls, function(url, callback){
        request(url, function (error, response, body) {
            callback(error, body);
        });
    }, function(err, results){
        console.log(results);
        //var jsonObject = JSON.parse(results);
        res.json(results);
    });

    // Super fake

    console.log("country >>"+req.body.country);
    console.log("city >>"+req.body.city);

    res.status(200);


};

function handlePutOrders(req, res) {
    res.status(405).send("Cannot overwrite the entire collection.");
};


function handlePostOrders(req, res) {

    console.log("req");
    console.log(req.body);

    var apiKey = '1c5b49f9c1fa2deb';
    var lang = req.body.lang ? req.body.lang : 'EN';
    var idEncomenda = req.body.id_encomenda ? req.body.id_encomenda : 1;
    var country = req.body.country ? req.body.country : 'Portugal';
    var city = req.body.city ? req.body.city : 'Porto';

    var url = "http://api.wunderground.com/api/" + apiKey + "/conditions/" + lang + "/q/" + country + "/" + city + ".json";

    console.log(url);
    
    setTimeout(function(){
    		//SIMULAR ATRASO NO PROCESSO DE ENTREGA
		    var weatherStatus = '';
		
		    var http = require("http");
		
		    var request = http.get(url, function (response) {
		
		        var buffer = "",
		            data;
		        response.on("data", function (chunk) {
		            buffer += chunk;
		        });
		
		        response.on("end", function (err) {
		
		            //console.log(buffer);
		            //console.log("\n");
		            data = JSON.parse(buffer);
		
		            //res.json(data);
		
		            weatherStatus = data.current_observation.weather;
		
		            var previsao = mapWeatherStatus(weatherStatus);
		
		
		            var previsaoEntrega = {
		                id_encomenda : idEncomenda,
		                previsao_dias : previsao
		            }
		
		            //res.status(200).send(previsaoEntrega);
		            console.log("Indicar ao backoffice que o serviço se encontra em andamento. "+getDateTime());
		                    var requestCallback = require('request');
			                var bufferCallback = "";
			                console.log("POSTING TO BO CALLBACK@"+getDateTime());	                
			            	requestCallback({
			          		  uri: req.body.callbackUrl,
			          		  method: req.body.callbackMethod,  
			          		  form:{id_encomenda : req.body.order_id, confirmed : 3 , address : req.body.city , callbackMethod : req.body.callbackMethod , callbackUrl : req.body.callbackUrl, authToken : ""+authEncode(backofficePw + req.body.order_id) }
			          		}, function(error, response, body) {
				                console.log("GETTING BO CALLBACK ORDER RESPONSE@"+getDateTime());
				                console.log("body");
			          		    console.log(body);
		
			          		});
		            
		
		        });
		    });
		    request.end();
		    //ACABAR SIMULAÇÃO DE ATRASO NO PROCESSO DE ENTREGA
	}, 2500);   
		    
    
    //SIMULAR ATRASO DE ENTREGA AO CLIENTE
    setTimeout(function(){
        console.log("Indicar ao backoffice que o serviço foi entregue ao cliente. "+getDateTime());
        var requestCallback = require('request');
        var bufferCallback = "";
        console.log("POSTING TO BO CALLBACK@"+getDateTime());	                
    	requestCallback({
  		  uri: req.body.callbackUrl,
  		  method: req.body.callbackMethod,  
  		  form:{id_encomenda : req.body.order_id, confirmed : 4 , address : req.body.city , callbackMethod : req.body.callbackMethod , callbackUrl : req.body.callbackUrl , authToken : ""+authEncode(backofficePw + req.body.order_id)}
  		}, function(error, response, body) {
            console.log("GETTING BO CALLBACK ORDER RESPONSE@"+getDateTime());
            console.log("body");
  		    console.log(body);

  		});

	}, 11500);
    //FIM DE SIMULAR ATRASO NA ENTREGA AO CLIENTE
    
    var resposta = {
            id_encomenda : req.body.order_id,
            respostaStr : "Indicar ao printershop que o serviço foi recebido. "+getDateTime(),
            previsao_dias : 0,
            authToken : ""+authEncode(backofficePw + req.body.order_id)
        }
    
    //ENTRETANTO RESPONDER AO PRINTERSHOP
    console.log("Indicar ao printershop que o serviço foi recebido. "+getDateTime());
    res.status(200).send(resposta);	        
};


function getDateTime() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;

}

/*
function handlePostOrders(req, res) {

    console.log("req");
    console.log(req.body);

    var apiKey = '1c5b49f9c1fa2deb';
    var lang = req.body.lang ? req.body.lang : 'EN';
    var idEncomenda = req.body.id_encomenda ? req.body.id_encomenda : 1;
    var country = req.body.country ? req.body.country : 'Portugal';
    var city = req.body.city ? req.body.city : 'Porto';

    var url = "http://api.wunderground.com/api/" + apiKey + "/conditions/" + lang + "/q/" + country + "/" + city + ".json";

    console.log(url);
    
    
    		//SIMULAR ATRASO NO PROCESSO DE ENTREGA
		    var weatherStatus = '';
		
		    var http = require("http");
		
		    var request = http.get(url, function (response) {
		
		        var buffer = "",
		            data;
		        response.on("data", function (chunk) {
		            buffer += chunk;
		        });
		
		        response.on("end", function (err) {
		
		            //console.log(buffer);
		            //console.log("\n");
		            data = JSON.parse(buffer);
		
		            //res.json(data);
		
		            weatherStatus = data.current_observation.weather;
		
		            var previsao = mapWeatherStatus(weatherStatus);
		
		
		            var previsaoEntrega = {
		                id_encomenda : idEncomenda,
		                previsao_dias : previsao
		            }
		
		            res.status(200).send(previsaoEntrega);
		
		        });
		    });
		    request.end();
		    //ACABAR SIMULAÇÃO DE ATRASO NO PROCESSO DE ENTREGA
		    
		    
		    
		    
};
*/


function mapWeatherStatus(weather){
    var clear = 'Clear';
    var rain = 'Rain';

    var previsaoDias = 0;

    if (weather.indexOf(clear) >= 0) {
        previsaoDias = 2;
    }
    else if(weather.indexOf(rain) >= 0){
        previsaoDias = 5;
    }
    else{
        previsaoDias = 15;
    }

    return previsaoDias;
}

function handleDeleteOrders(req, res) {
    res.status(405).send("Cannot delete the entire collection.");
};

//
// handling individual itens in the collection
//
// message id must be present in the request object
//
// GET 		return specific message or 404
// POST 	update existing entry or 404
// PUT 		overwrite existing or create new given the id.
// DELETE 	deletes the message
//

function handleGetOrderItem(req, res) {
    var entry = messages[req.messageID];
    console.log("»» requested: " + req.messageID + " »» " + entry);
    if (entry === undefined) {
        res.status(404).send("Order " + req.messageID + " not found.");
    }
    else {
        res.json(entry);
    }
};

function handlePutOrderItem(req, res) {
    var entry = messages[req.messageID];
    if (entry === undefined) {
        entry = buildOrder(req.messageID, req.body.text, req.body.user);
        messages[req.messageID] = entry;
        res.status(201).set('Location', SERVER_ROOT + "/message/" + req.messageID).json(entry);
    }
    else {
        entry.text = req.body.text;
        entry.updatedOn = new Date();
        res.json(entry);
    }
};

function handlePostOrderItem(req, res) {
    var entry = messages[req.messageID];
    if (entry === undefined) {
        res.status(404).send("Order " + req.messageID + " not found.");
    }
    else {
        entry.text = req.body.text;
        var now = new Date();
        entry.updatedOn = now;
        res.json(entry);
    }
};

function handleDeleteOrderItem(req, res) {
    var entry = messages[req.messageID];
    if (entry === undefined) {
        res.status(404).send("Order " + req.messageID + " not found.");
    }
    else {
        delete messages[req.messageID];
        res.status(204).send("Order " + req.messageID + " deleted.");
    }
};

function authEncode(cleanStr){
	
	return new Buffer(cleanStr).toString('base64');
	
}

function authDecode(dirtyStr){
	
	return new Buffer(dirtyStr, 'base64').toString('ascii')
}

/////////////////////////////
// MODULE EXPORTS

exports.handleGetOrders = handleGetOrders;
exports.handlePutOrders = handlePutOrders;
exports.handlePostOrders = handlePostOrders;
exports.handleDeleteOrders = handleDeleteOrders;

exports.handleGetOrderItem = handleGetOrderItem;
exports.handlePostOrderItem = handlePostOrderItem;
exports.handlePutOrderItem = handlePutOrderItem;
exports.handleDeleteOrderItem = handleDeleteOrderItem;
